import Api from '@/services/Api'

export default {
  fetchStudents (page = 1, rows = 6, {firstName, lastName, email, city, region}) {
    return Api().get(`api/v1/students?page=${page - 1}&rows=${rows}&firstName=${firstName}&lastName=${lastName}&email=${email}&city=${city}&region=${region}`)
  }
}
