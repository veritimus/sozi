import Api from '@/services/Api'

export default {
  checkIfExist (username, email) {
    return Api().get(`auth?username=${username}&email=${email}`)
  },

  generatePassword (username) {
    return Api().post('auth/user/generate_password', {username})
  },

  generateToken (email) {
    return Api().post('auth/user/generate_token', {email})
  },

  login (credentials) {
    return Api().post('auth/login', credentials)
  },

  register (credentials) {
    return Api().post('auth/register', credentials)
  },

  verify (token) {
    return Api().get(`auth/user/${token}`)
  }
}
