import Api from '@/services/Api'

export default {
  fetchSubjects (name) {
    return Api().get(`api/v1/subjects?name=${name}`)
  }
}
