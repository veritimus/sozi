import Api from '@/services/Api'

export default {
  createChildAccount (userId, formData) {
    return Api().post(`api/v1/users/${userId}/children`, formData)
  },

  fetchChildren (userId) {
    return Api().get(`api/v1/users/${userId}/children`)
  },

  fetchChildrenLessonsBetweenDates (userId, {startDate, endDate, status}) {
    return Api().get(`api/v1/users/${userId}/childrenLessons?startDate=${startDate}&endDate=${endDate}&status=${status}`)
  },

  fetchLessonsBetweenDates (userId, {startDate, endDate, status}) {
    return Api().get(`api/v1/users/${userId}/lessons?startDate=${startDate}&endDate=${endDate}&status=${status}`)
  },

  fetchUser (userId) {
    return Api().get(`api/v1/users/${userId}`)
  },

  fetchUserUnit (userId, unitId) {
    return Api().get(`api/v1/users/${userId}/eduUnits/${unitId}`)
  },

  fetchUserUnits (userId) {
    return Api().get(`api/v1/users/${userId}/eduUnits`)
  },

  updateUser (userId, updateData) {
    return Api().put(`api/v1/users/${userId}`, updateData)
  }
}
