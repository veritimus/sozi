import axios from 'axios'
import store from '@/store'

export default () => {
  const instance = axios.create({
    baseURL: process.env.VUE_APP_API_URL,
    headers: {
      Authorization: `Bearer ${store.state.auth.token}`,
      Language: store.state.locale.lang
    }
  })
  instance.interceptors.response.use(response => response, error => {
    if (error.response.status === 401 && (!error.response.data.error || error.response.data.error.code !== 'ErrorInvalidCredentials')) {
      store.dispatch('auth/logout')
    }
    return Promise.reject(error)
  })
  return instance
}
