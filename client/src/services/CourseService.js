import Api from '@/services/Api'

export default {
  createLesson (courseId, formData) {
    return Api().post(`api/v1/courses/${courseId}/lessons`, formData)
  },

  fetchCourse (courseId) {
    return Api().get(`api/v1/courses/${courseId}`)
  },

  fetchCourses (page = 1, rows = 6, {title, subjectName, minPrice, maxPrice, unitName, city, region, types}) {
    return Api().get(`api/v1/courses?page=${page - 1}&rows=${rows}&title=${title}&subject=${subjectName}&minPrice=${minPrice}&maxPrice=${maxPrice}&unit=${unitName}&city=${city}&region=${region}&types=${types}`)
  },

  fetchStudents (courseId, status = 'active') {
    return Api().get(`api/v1/courses/${courseId}/students?status=${status}`)
  },

  updateCourse (courseId, updateData) {
    return Api().put(`api/v1/courses/${courseId}`, updateData)
  }
}
