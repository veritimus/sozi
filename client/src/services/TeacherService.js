import Api from '@/services/Api'

export default {
  fetchLessonsBetweenDates (userId, {startDate, endDate, status}) {
    return Api().get(`api/v1/teachers/${userId}/lessons?startDate=${startDate}&endDate=${endDate}&status=${status}`)
  },

  fetchUnits (userId, offset, rowsQuantity) {
    return Api().get(`api/v1/teachers/${userId}/eduUnits?offset=${offset}&rows=${rowsQuantity}`)
  }
}
