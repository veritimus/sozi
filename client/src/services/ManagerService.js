import Api from '@/services/Api'

export default {
  fetchUnits (userId, offset = null, rowsQuantity = null) {
    return Api().get(`api/v1/managers/${userId}/eduUnits?offset=${offset}&rows=${rowsQuantity}`)
  }
}
