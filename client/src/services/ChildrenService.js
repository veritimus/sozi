import Api from '@/services/Api'

export default {
  fetchStudents (offset, rows, {firstName, lastName, email, city, region}) {
    return Api().get(`api/v1/children?offset=${offset}&rows=${rows}&firstName=${firstName}&lastName=${lastName}&email=${email}&city=${city}&region=${region}`)
  }
}
