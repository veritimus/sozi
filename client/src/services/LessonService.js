import Api from '@/services/Api'

export default {
  fetchLesson (lessonId) {
    return Api().get(`api/v1/lessons/${lessonId}`)
  },

  updateLesson (lessonId, updateData) {
    return Api().put(`api/v1/lessons/${lessonId}`, updateData)
  }
}
