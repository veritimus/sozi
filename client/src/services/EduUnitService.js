import Api from '@/services/Api'

export default {
  addStudent (unitId, userId, userType) {
    return Api().post(`api/v1/eduUnits/${unitId}/students`, {userId, userType})
  },

  addTeacher (unitId, userId) {
    return Api().post(`api/v1/eduUnits/${unitId}/teachers`, {userId})
  },

  changeCourseStatus (unitId, courseId, updateData) {
    return Api().put(`api/v1/eduUnits/${unitId}/courses/${courseId}`, updateData)
  },

  changeStudentStatus (unitId, userId, updateData) {
    return Api().put(`api/v1/eduUnits/${unitId}/students/${userId}`, updateData)
  },

  changeTeacherStatus (unitId, userId, updateData) {
    return Api().put(`api/v1/eduUnits/${unitId}/teachers/${userId}`, updateData)
  },

  createCourse (unitId, formData) {
    return Api().post(`api/v1/eduUnits/${unitId}/courses`, formData)
  },

  createLessons (unitId, courseId, formData) {
    return Api().post(`api/v1/eduUnits/${unitId}/courses/${courseId}/lessons`, formData)
  },

  createUnit (formData) {
    return Api().post(`api/v1/eduUnits`, formData)
  },

  fetchCoursesByStatus (unitId, status) {
    return Api().get(`api/v1/eduUnits/${unitId}/courses?status=${status}`)
  },

  fetchCoursesByTeacher (unitId, userId, status = 'active') {
    return Api().get(`api/v1/eduUnits/${unitId}/teachers/${userId}/courses?status=${status}`)
  },

  fetchLessonsBetweenDates (unitId, {startDate, endDate, status}) {
    return Api().get(`api/v1/eduUnits/${unitId}/lessons?startDate=${startDate}&endDate=${endDate}&status=${status}`)
  },

  fetchStudentsByLastName (unitId, lastName) {
    return Api().get(`api/v1/eduUnits/${unitId}/students?lastName=${lastName}`)
  },

  fetchStudentsByStatus (unitId, status) {
    return Api().get(`api/v1/eduUnits/${unitId}/students?status=${status}`)
  },

  fetchTeachers (unitId) {
    return Api().get(`api/v1/eduUnits/${unitId}/teachers`)
  },

  fetchUnit (unitId) {
    return Api().get(`api/v1/eduUnits/${unitId}`)
  },

  fetchUnits (page = 1, rows = 6, {name, email, city, region, address, types}) {
    return Api().get(`api/v1/eduUnits?page=${page - 1}&rows=${rows}&name=${name}&email=${email}&city=${city}&region=${region}&address=${address}&types=${types}`)
  },

  updateUnit (unitId, updateData) {
    return Api().put(`api/v1/eduUnits/${unitId}`, updateData)
  }
}
