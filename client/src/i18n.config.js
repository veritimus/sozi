import Vue from 'vue'
import VueI18n from 'vue-i18n'
import messages from '@/locale/lang/pl.json'

Vue.use(VueI18n)

export const i18n = new VueI18n({
  locale: 'pl',
  fallbackLocale: 'pl',
  messages: messages
})

export const availableLanguages = ['en', 'pl']
const loadedLanguages = ['pl']

function setI18nLanguage (lang) {
  i18n.locale = lang
  document.querySelector('html').setAttribute('lang', lang)
  localStorage.setItem('lang', lang)
  return lang
}

export function loadLanguageAsync (lang) {
  if (i18n.locale !== lang) {
    if (!loadedLanguages.includes(lang)) {
      return import(`./locale/lang/${lang}.json`).then(messages => {
        i18n.setLocaleMessage(lang, messages.default[lang])
        loadedLanguages.push(lang)
        return setI18nLanguage(lang)
      })
    }

    return Promise.resolve(setI18nLanguage(lang))
  }

  return Promise.resolve(lang)
}
