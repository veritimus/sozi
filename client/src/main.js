import Vue from 'vue'
import {i18n} from '@/i18n.config'
import App from '@/App.vue'
import router from '@/router'
import store from '@/store'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import VueMoment from 'vue-moment'
import moment from 'moment-timezone'

moment.tz.setDefault('Europe/Warsaw')

Vue.config.productionTip = false

Vue.use(Vuetify, {
  iconfont: 'fa',
  theme: {
    primary: '#303F9F',
    secondary: '#E8EAF6',
    accent: '#1A237E',
    error: '#e53935',
    error_bg: '#ffcdd2',
    warning: '#f9a825',
    warning_bg: '#fff9c4',
    info: '#3949ab',
    info_bg: '#c5cae9',
    success: '#558b2f',
    success_bg: '#dcedc8'
  }
})
Vue.use(VueMoment, {
  moment
})

new Vue({
  i18n,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
