import moment from 'moment'

export const lessonDate = date => date ? moment(date).format('H:mm, DD.MM.YYYY') : '-----'
export const lessonTimePeriod = (date, duration) => date && duration ? `${moment(date).format('H:mm')} – ${moment(date).add(duration, 'minutes').format('H:mm')}` : '-----'
export const paymentDeadline = date => date ? moment(date).format('H:mm, DD.MM.YYYY') : '-----'
