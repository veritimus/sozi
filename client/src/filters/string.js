const formatBankAccount = (number, result, index) => index + 4 < 26 ? formatBankAccount(number, `${result} ${number.substring(index, index + 4)}`, index + 4) : `${result} ${number.substring(index, index + 4)}`

export const bankAccount = number => number ? formatBankAccount(number, number.substring(0, 2), 2) : '-----'
export const maxLengthString = (string, maxLength) => string.length > maxLength ? string.substring(0, maxLength + 1).concat('...') : string
export const personName = person => person ? `${person.firstName} ${person.lastName}` : '-----'
