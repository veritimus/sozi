import {i18n} from '@/i18n.config'

export default {
  namespaced: true,
  state: {
    lang: localStorage.getItem('lang') || i18n.locale
  },
  mutations: {
    setLang (state, lang) {
      state.lang = lang
    }
  },
  actions: {
    setLang ({commit}, lang) {
      commit('setLang', lang)
      localStorage.setItem('lang', lang)
    }
  },
  getters: {
    lang: state => state.lang
  }
}
