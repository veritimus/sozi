import router from '@/router'
import UserService from '@/services/UserService'

export default {
  namespaced: true,
  state: {
    user: null
  },
  mutations: {
    clearUser (state) {
      state.user = null
    },
    setUser (state, user) {
      state.user = user
    }
  },
  actions: {
    fetchUser ({commit, dispatch}, id) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await UserService.fetchUser(id)
          commit('setUser', data.user)
          if (data.user.children) {
            commit('children/setChildren', {
              children: data.user.children,
              totalChildren: data.user.children.length
            }, {root: true})
          }
          resolve(data.user)
        } catch (error) {
          await dispatch('auth/logout', null, {root: true})
          router.go()
          reject(error.response.data.error)
        }
      })
    }
  },
  getters: {
    user: state => state.user,
    roles: state => state.user ? state.user.Roles.map(role => role.name) : []
  }
}
