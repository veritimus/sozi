import UserService from '@/services/UserService'

export default {
  namespaced: true,
  state: {
    children: [],
    totalChildren: 0
  },
  mutations: {
    clearChildren (state) {
      state.children = []
      state.totalChildren = 0
    },
    pushNewChild (state, child) {
      state.children.push(child)
      state.totalChildren += 1
    },
    setChildren (state, {children, totalChildren}) {
      state.children = children
      state.totalChildren = totalChildren
    }
  },
  actions: {
    createChild ({commit, state, rootState}, formData) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await UserService.createChildAccount(rootState.auth.userId, formData)
          commit('pushNewChild', data.child)
          resolve({message: data.message, type: data.type})
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    },
    fetchChildren ({commit, rootState}) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await UserService.fetchChildren(rootState.auth.userId)
          commit('setChildren', data)
          resolve({children: data.children, totalChildren: data.totalChildren})
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    }
  },
  getters: {
    children: state => state.children,
    pageChildren: state => (page, rowsPerPage) => page > 1 ? state.children.slice(((page - 1) * rowsPerPage) - 1, (page * rowsPerPage) - 1) : state.children.slice(0, 5),
    paginationLength: state => rowsPerPage => Math.ceil((state.totalChildren + 1) / rowsPerPage),
    totalChildren: state => state.totalChildren
  }
}
