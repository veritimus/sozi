import router from '@/router'
import AuthenticationService from '@/services/AuthenticationService'

export default {
  namespaced: true,
  state: {
    userId: null,
    token: null
  },
  mutations: {
    clearAuth (state) {
      state.userId = null
      state.token = null
    },
    setAuth (state, {id, token}) {
      state.userId = id
      state.token = token
    }
  },
  actions: {
    login ({commit, dispatch}, {username, password}) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data: {user}} = await AuthenticationService.login({username, password})
          commit('setAuth', {id: user.id, token: user.token})
          localStorage.setItem('user-id', user.id)
          localStorage.setItem('user-token', user.token)
          await dispatch('user/fetchUser', user.id, {root: true})
          await dispatch('setLogoutTimer', user.expiresIn)
          resolve()
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    },
    logout ({commit, dispatch}) {
      commit('clearAuth')
      commit('user/clearUser', null, {root: true})
      commit('children/clearChildren', null, {root: true})
      dispatch('lessons/clearLessons', null, {root: true})
      dispatch('educationalUnits/clearEduUnits', null, {root: true})
      localStorage.removeItem('user-id')
      localStorage.removeItem('user-token')
      router.go()
    },
    createUser ({commit}, formData) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await AuthenticationService.register(formData)
          resolve({message: data.message, type: data.type})
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    },
    setAuth ({commit}, localUserData) {
      commit('setAuth', localUserData)
    },
    setLogoutTimer ({commit, dispatch}, seconds) {
      setTimeout(async () => {
        await dispatch('logout')
      }, seconds * 1000)
    }
  },
  getters: {
    isAuthenticated: state => state.token !== null,
    userId: state => state.userId
  }
}
