import managerStudents from '@/store/modules/students/managerStudents'
import searchStudents from '@/store/modules/students/searchStudents'
import teacherStudents from '@/store/modules/students/teacherStudents'

export default {
  namespaced: true,
  modules: {
    managerStudents,
    searchStudents,
    teacherStudents
  },
  actions: {
    clearEduUnits ({commit}) {
      commit('managerStudents/clearStudents')
      commit('searchStudents/clearStudents')
      commit('teacherStudents/clearStudents')
    }
  }
}
