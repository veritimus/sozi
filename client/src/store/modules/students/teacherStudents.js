import EduUnitService from '@/services/EduUnitService'
import TeacherService from '@/services/TeacherService'

export default {
  namespaced: true,
  state: {
    students: [],
    units: [],
    totalStudents: 0
  },
  mutations: {
    clearStudents (state) {
      state.students = []
      state.units = []
      state.totalUnits = 0
    },
    setStudents (state, {students, totalStudents}) {
      state.students = students
      state.totalStudents = totalStudents
    },
    setUnits (state, units) {
      state.units = units
    }
  },
  actions: {
    fetchStudents ({commit}, {unitId, status}) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await EduUnitService.fetchStudentsByStatus(unitId, status)
          commit('setStudents', data)
          resolve({students: data.students, totalStudents: data.totalStudents})
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    },
    fetchUnits ({commit, rootState}) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await TeacherService.fetchUnits(rootState.auth.userId, null, null)
          commit('setUnits', data.units)
          resolve()
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    }
  },
  getters: {
    students: state => state.students,
    pageStudents: state => (page, rowsPerPage) => state.students.slice((page - 1) * rowsPerPage, page * rowsPerPage),
    paginationLength: state => rowsPerPage => Math.ceil(state.totalStudents / rowsPerPage),
    totalStudents: state => state.totalStudents,
    units: state => state.units
  }
}
