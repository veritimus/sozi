import StudentService from '@/services/StudentService'

export default {
  namespaced: true,
  state: {
    students: [],
    totalStudents: 0
  },
  mutations: {
    clearStudents (state) {
      state.students = []
      state.totalStudents = 0
    },
    changeStatus (state, {index, student}) {
      state.students.splice(index, 1, student)
    },
    setStudents (state, {students, totalStudents}) {
      state.students = students
      state.totalStudents = totalStudents
    }
  },
  actions: {
    fetchStudents ({commit}, {page, rows, formData}) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await StudentService.fetchStudents(page, rows, formData)
          commit('setStudents', data)
          resolve({students: data.students, totalStudents: data.totalStudents})
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    }
  },
  getters: {
    students: state => state.students,
    paginationLength: state => rowsPerPage => Math.ceil(state.totalStudents / rowsPerPage),
    totalStudents: state => state.totalStudents
  }
}
