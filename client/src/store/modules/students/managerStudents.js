import EduUnitService from '@/services/EduUnitService'
import ManagerService from '@/services/ManagerService'

export default {
  namespaced: true,
  state: {
    students: [],
    units: [],
    totalStudents: 0
  },
  mutations: {
    clearStudents (state) {
      state.students = []
      state.units = []
      state.totalUnits = 0
    },
    changeStatus (state, {index, student}) {
      state.students.splice(index, 1, student)
    },
    removeStudent (state, studentId) {
      state.students.splice(state.students.indexOf(state.students.find(student => student.id === studentId)), 1)
      state.totalStudents -= 1
    },
    setStudents (state, {students, totalStudents}) {
      state.students = students
      state.totalStudents = totalStudents
    },
    setUnits (state, units) {
      state.units = units
    }
  },
  actions: {
    changeStatus ({commit, state, rootState}, {studentId, unitId, status, listStatus}) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await EduUnitService.changeStudentStatus(unitId, studentId, {status, updater: rootState.auth.userId})
          if (listStatus === 'all') {
            const index = state.students.indexOf(state.students.find(student => student.id === studentId))
            const student = state.students[index]
            student['schools'][0]['school']['status'] = status
            commit('changeStatus', {index, student})
          } else {
            commit('removeStudent', studentId)
          }
          resolve({message: data.message, type: data.type})
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    },
    fetchStudents ({commit}, {unitId, status}) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await EduUnitService.fetchStudentsByStatus(unitId, status)
          commit('setStudents', data)
          resolve({students: data.students, totalStudents: data.totalStudents})
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    },
    fetchUnits ({commit, rootState}) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await ManagerService.fetchUnits(rootState.auth.userId, null, null)
          commit('setUnits', data.units)
          resolve()
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    }
  },
  getters: {
    students: state => state.students,
    pageStudents: state => (page, rowsPerPage) => state.students.slice((page - 1) * rowsPerPage, page * rowsPerPage),
    paginationLength: state => rowsPerPage => Math.ceil(state.totalStudents / rowsPerPage),
    totalStudents: state => state.totalStudents,
    units: state => state.units
  }
}
