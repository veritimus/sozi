import childrenUnits from '@/store/modules/educationalUnits/childrenUnits'
import managerUnits from '@/store/modules/educationalUnits/managerUnits'
import myUnits from '@/store/modules/educationalUnits/myUnits'
import teacherUnits from '@/store/modules/educationalUnits/teacherUnits'
import searchUnits from '@/store/modules/educationalUnits/searchUnits'

export default {
  namespaced: true,
  modules: {
    childrenUnits,
    managerUnits,
    myUnits,
    teacherUnits,
    searchUnits
  },
  actions: {
    clearEduUnits ({commit}) {
      commit('childrenUnits/clearUnits')
      commit('managerUnits/clearUnits')
      commit('myUnits/clearUnits')
      commit('teacherUnits/clearUnits')
      commit('searchUnits/clearUnits')
    }
  }
}
