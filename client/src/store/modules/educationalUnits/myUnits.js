import EduUnitService from '@/services/EduUnitService'
import UserService from '@/services/UserService'

export default {
  namespaced: true,
  state: {
    units: [],
    totalUnits: 0
  },
  mutations: {
    clearUnits (state) {
      state.units = []
      state.totalUnits = 0
    },
    removeUnit (state, unitId) {
      state.units.splice(state.units.indexOf(state.units.find(unit => unit.id === unitId)), 1)
      state.totalUnits -= 1
    },
    setUnits (state, {units, totalUnits}) {
      state.units = units
      state.totalUnits = totalUnits
    }
  },
  actions: {
    async changeStatus ({commit, state, rootState}, {unitId, status}) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await EduUnitService.changeStudentStatus(unitId, rootState.auth.userId, {status, updater: rootState.auth.userId})
          commit('removeUnit', unitId)
          resolve({message: data.message, type: data.type})
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    },
    fetchUnits ({commit, rootState}) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await UserService.fetchUserUnits(rootState.auth.userId)
          commit('setUnits', data)
          resolve({units: data.units, totalUnits: data.totalUnits})
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    }
  },
  getters: {
    units: state => state.units,
    pageUnits: state => (page, rowsPerPage) => state.units.slice((page - 1) * rowsPerPage, page * rowsPerPage),
    paginationLength: state => rowsPerPage => Math.ceil(state.totalUnits / rowsPerPage),
    totalUnits: state => state.totalUnits
  }
}
