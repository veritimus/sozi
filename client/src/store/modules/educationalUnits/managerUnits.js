import EduUnitService from '@/services/EduUnitService'
import ManagerService from '@/services/ManagerService'

export default {
  namespaced: true,
  state: {
    units: [],
    unitsIdList: null,
    unitsStudents: null,
    unitsTeachers: null,
    totalUnits: 0
  },
  mutations: {
    clearUnits (state) {
      state.units = []
      state.unitsIdList = null
      state.unitsStudents = null
      state.unitsTeachers = null
      state.totalUnits = 0
    },
    incrementTotalUnits (state) {
      state.totalUnits += 1
    },
    pushNewUnit (state, unit) {
      state.units.push(unit)
      state.unitsIdList[unit.id] = {students: {active: [], pending: []}, teachers: {active: [], pending: []}}
      state.totalUnits += 1
    },
    pushActiveItemToUnitList (state, {itemId, unitId, itemType}) {
      state.unitsIdList[unitId][`${itemType}s`].active.push(itemId)
    },
    removePendingItemFromUnitList (state, {itemId, unitId, itemType}) {
      state.unitsIdList[unitId][`${itemType}s`].pending.splice(state.unitsIdList[unitId][`${itemType}s`].pending.indexOf(itemId), 1)
    },
    setStudentStatus (state, {studentId, unitId, status}) {
      state.unitsStudents[unitId].find(student => student.id === studentId).StudentEducationalUnit.status = status
    },
    setTeacherStatus (state, {teacherId, unitId, status}) {
      state.unitsTeachers[unitId].find(teacher => teacher.id === teacherId).TeacherEducationalUnit.status = status
    },
    setUnits (state, {units, unitsIdList, unitsStudents, unitsTeachers, totalUnits}) {
      state.units = units
      state.unitsIdList = unitsIdList
      state.unitsStudents = unitsStudents
      state.unitsTeachers = unitsTeachers
      state.totalUnits = totalUnits
    }
  },
  actions: {
    changeStatus ({commit, state, rootState}, {itemId, unitId, itemType, status}) {
      return new Promise(async (resolve, reject) => {
        try {
          let response
          if (itemType === 'student') {
            response = await EduUnitService.changeStudentStatus(unitId, itemId, {status, updater: rootState.auth.userId})
            commit('setStudentStatus', {studentId: itemId, unitId, status})
          } else if (itemType === 'teacher') {
            response = await EduUnitService.changeTeacherStatus(unitId, itemId, {status, updater: rootState.auth.userId})
            commit('setTeacherStatus', {teacherId: itemId, unitId, status})
          }
          commit('removePendingItemFromUnitList', {itemId, unitId, itemType})
          if (status === 'active') {
            commit('pushActiveItemToUnitList', {itemId, unitId, itemType})
          }
          resolve({message: response.data.message, type: response.data.type})
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    },
    createUnit ({commit, state}, formData) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await EduUnitService.createUnit(formData)
          if (data.unit) {
            if (state.units.length < 5) {
              commit('pushNewUnit', data.unit)
            } else {
              commit('incrementTotalUnits')
            }
          }
          resolve({message: data.message, type: data.type})
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    },
    fetchUnits ({commit, rootState}, {page, rows}) {
      return new Promise(async (resolve, reject) => {
        try {
          let offset, rowsQuantity
          if (page === 1) {
            offset = 0
            rowsQuantity = rows - 1
          } else {
            offset = ((page - 1) * rows) - 1
            rowsQuantity = rows
          }
          const {data} = await ManagerService.fetchUnits(rootState.auth.userId, offset, rowsQuantity)
          commit('setUnits', data)
          resolve({units: data.units, unitsIdList: data.unitsIdList, unitsTeachers: data.unitsTeachers, unitsStudents: data.unitsStudents, totalUnits: data.totalUnits})
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    }
  },
  getters: {
    units: state => state.units,
    paginationLength: state => rowsPerPage => Math.ceil((state.totalUnits + 1) / rowsPerPage),
    unitsIdListActiveItems: state => (unitId, itemsType) => state.unitsIdList[unitId][itemsType].active.length,
    unitsIdListPendingItems: state => (unitId, itemsType) => state.unitsIdList[unitId][itemsType].pending.length,
    unitsItemsActive: state => (unitId, itemsType) => state.unitsIdList[unitId][itemsType].active.slice(0, 5).map(itemId => itemsType === 'students'
      ? state.unitsStudents[unitId].find(student => student.id === itemId)
      : state.unitsTeachers[unitId].find(teacher => teacher.id === itemId)
    ),
    unitsItemsPending: state => (unitId, itemsType) => state.unitsIdList[unitId][itemsType].pending.slice(0, 5).map(itemId => itemsType === 'students'
      ? state.unitsStudents[unitId].find(student => student.id === itemId)
      : state.unitsTeachers[unitId].find(teacher => teacher.id === itemId)
    ),
    unitsStudents: state => unitId => state.unitsStudents[state.unitsIdList.indexOf(unitId)].filter(student => student.StudentEducationalUnit.status === 'pending').slice(0, 5),
    totalUnits: state => state.totalUnits
  }
}
