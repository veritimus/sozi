import EduUnitService from '@/services/EduUnitService'

export default {
  namespaced: true,
  state: {
    units: [],
    totalUnits: 0
  },
  mutations: {
    clearUnits (state) {
      state.units = []
      state.totalUnits = 0
    },
    setUnits (state, {units, totalUnits}) {
      state.units = units
      state.totalUnits = totalUnits
    }
  },
  actions: {
    fetchUnits ({commit}, {page, rows, formData}) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await EduUnitService.fetchUnits(page, rows, formData)
          commit('setUnits', data)
          resolve({units: data.units, totalUnits: data.totalUnits})
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    }
  },
  getters: {
    units: state => state.units,
    paginationLength: state => rowsPerPage => Math.ceil(state.totalUnits / rowsPerPage),
    totalUnits: state => state.totalUnits
  }
}
