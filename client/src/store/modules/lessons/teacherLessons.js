import CourseService from '@/services/CourseService'
import EduUnitService from '@/services/EduUnitService'
import TeacherService from '@/services/TeacherService'

export default {
  namespaced: true,
  state: {
    courses: [],
    lessons: [],
    students: [],
    units: []
  },
  mutations: {
    clearLessons (state) {
      state.courses = []
      state.lessons = []
      state.students = []
      state.units = []
    },
    pushNewLesson (state, lesson) {
      state.lessons.push(lesson)
    },
    setCourses (state, courses) {
      state.courses = courses
    },
    setLessons (state, {lessons, units}) {
      state.lessons = lessons
      state.units = units
    },
    setStudents (state, students) {
      state.students = students
    }
  },
  actions: {
    createLesson ({commit, state}, formData) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await CourseService.createLesson(formData.courseId, formData)
          commit('pushNewLesson', data.lesson)
          resolve({message: data.message, type: data.type})
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    },
    fetchCourses ({commit, state, rootState}, unitId) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await EduUnitService.fetchCoursesByTeacher(unitId, rootState.auth.userId)
          commit('setCourses', data.courses)
          resolve({courses: data.courses})
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    },
    fetchLessons ({commit, rootState}, queryData) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await TeacherService.fetchLessonsBetweenDates(rootState.auth.userId, queryData)
          const response = await TeacherService.fetchUnits(rootState.auth.userId)
          commit('setLessons', {lessons: data.lessons, units: response.data.units})
          resolve({lessons: data.lessons})
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    },
    setStudents ({commit}, students) {
      commit('setStudents', students)
    }
  },
  getters: {
    courses: state => state.courses,
    lessons: state => state.lessons,
    students: state => state.students,
    units: state => state.units
  }
}
