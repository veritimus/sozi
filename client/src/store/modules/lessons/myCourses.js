import EduUnitService from '@/services/EduUnitService'
import ManagerService from '@/services/ManagerService'

export default {
  namespaced: true,
  state: {
    courses: [],
    units: [],
    totalCourses: 0
  },
  mutations: {
    clearCourses (state) {
      state.courses = []
      state.units = []
      state.totalUnits = 0
    },
    changeStatus (state, {index, course}) {
      state.courses[index].isActive = course.isActive
    },
    incrementTotalCourses (state) {
      state.totalCourses += 1
    },
    pushNewCourse (state, course) {
      state.courses.push(course)
      state.totalCourses += 1
    },
    removeCourse (state, index) {
      state.courses.splice(index, 1)
      state.totalCourses -= 1
    },
    setCourses (state, {courses, totalCourses}) {
      state.courses = courses
      state.totalCourses = totalCourses
    },
    setUnits (state, units) {
      state.units = units
    }
  },
  actions: {
    changeStatus ({commit, state}, {courseId, listStatus, status, unitId}) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await EduUnitService.changeCourseStatus(unitId, courseId, {status})
          const index = state.courses.indexOf(state.courses.find(course => course.id === courseId))
          if (listStatus === 'all') {
            commit('changeStatus', {index, course: data.course})
          } else {
            commit('removeCourse', index)
          }
          resolve({message: data.message, type: data.type})
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    },
    createCourse ({commit, state}, {unitId, formData}) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await EduUnitService.createCourse(unitId, formData)
          if (state.courses.length < 10) {
            commit('pushNewCourse', data.course)
          } else {
            commit('incrementTotalCourses')
          }
          resolve({message: data.message, type: data.type})
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    },
    createLessons ({commit, state}, {unitId, courseId, formData}) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await EduUnitService.createLessons(unitId, courseId, formData)
          resolve({message: data.message, type: data.type})
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    },
    fetchCourses ({commit}, {unitId, status}) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await EduUnitService.fetchCoursesByStatus(unitId, status)
          commit('setCourses', data)
          resolve({courses: data.courses, totalCourses: data.totalCourses})
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    },
    fetchUnits ({commit, rootState}) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await ManagerService.fetchUnits(rootState.auth.userId)
          commit('setUnits', data.units)
          resolve()
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    }
  },
  getters: {
    courses: state => state.courses,
    pageCourses: state => (page, rowsPerPage) => state.courses.slice((page - 1) * rowsPerPage, page * rowsPerPage),
    paginationLength: state => rowsPerPage => Math.ceil((state.totalCourses + 1) / rowsPerPage),
    totalCourses: state => state.totalCourses,
    units: state => state.units
  }
}
