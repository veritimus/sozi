import CourseService from '@/services/CourseService'
import EduUnitService from '@/services/EduUnitService'
import ManagerService from '@/services/ManagerService'

export default {
  namespaced: true,
  state: {
    courses: [],
    lessons: [],
    students: [],
    units: []
  },
  mutations: {
    clearLessons (state) {
      state.courses = []
      state.lessons = []
      state.students = []
      state.units = []
    },
    pushNewLesson (state, lesson) {
      state.lessons.push(lesson)
    },
    setCourses (state, courses) {
      state.courses = courses
    },
    setLessons (state, lessons) {
      state.lessons = lessons
    },
    setStudents (state, students) {
      state.students = students
    },
    setUnits (state, units) {
      state.units = units
    }
  },
  actions: {
    createLesson ({commit, state}, formData) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await CourseService.createLesson(formData.courseId, formData)
          commit('pushNewLesson', data.lesson)
          resolve({message: data.message, type: data.type})
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    },
    fetchCourses ({commit, state, rootState}, unitId) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await EduUnitService.fetchCoursesByStatus(unitId, 'manager')
          commit('setCourses', data.courses)
          resolve({courses: data.courses})
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    },
    fetchLessons ({commit}, {unitId, queryData}) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await EduUnitService.fetchLessonsBetweenDates(unitId, queryData)
          commit('setLessons', data.lessons)
          resolve({lessons: data.lessons})
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    },
    fetchUnits ({commit, rootState}) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await ManagerService.fetchUnits(rootState.auth.userId)
          commit('setUnits', data.units)
          resolve({units: data.units})
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    },
    setStudents ({commit}, students) {
      commit('setStudents', students)
    }
  },
  getters: {
    courses: state => state.courses,
    lessons: state => state.lessons,
    students: state => state.students,
    units: state => state.units
  }
}
