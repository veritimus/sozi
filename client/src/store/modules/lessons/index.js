import childrenLessons from '@/store/modules/lessons/childrenLessons'
import myCourses from '@/store/modules/lessons/myCourses'
import myLessons from '@/store/modules/lessons/myLessons'
import searchCourses from '@/store/modules/lessons/searchCourses'
import teacherLessons from '@/store/modules/lessons/teacherLessons'
import unitLessonsSchedule from '@/store/modules/lessons/unitLessonsSchedule'

export default {
  namespaced: true,
  modules: {
    childrenLessons,
    myCourses,
    myLessons,
    searchCourses,
    teacherLessons,
    unitLessonsSchedule
  },
  actions: {
    clearLessons ({commit}) {
      commit('myCourses/clearCourses')
      // commit('childrenLessons/clearLessons')
      // commit('myLessons/clearLessons')
      commit('searchCourses/clearCourses')
      // commit('teacherLessons/clearLessons')
      // commit('unitLessonsSchedule/clearLessons')
    }
  }
}
