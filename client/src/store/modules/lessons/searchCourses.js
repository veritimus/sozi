import CourseService from '@/services/CourseService'

export default {
  namespaced: true,
  state: {
    courses: [],
    totalCourses: 0
  },
  mutations: {
    clearCourses (state) {
      state.courses = []
      state.totalCourses = 0
    },
    setCourses (state, {courses, totalCourses}) {
      state.courses = courses
      state.totalCourses = totalCourses
    }
  },
  actions: {
    fetchCourses ({commit}, {page, rows, formData}) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await CourseService.fetchCourses(page, rows, formData)
          commit('setCourses', data)
          resolve({courses: data.courses, totalCourses: data.totalCourses})
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    }
  },
  getters: {
    courses: state => state.courses,
    paginationLength: state => rowsPerPage => Math.ceil(state.totalCourses / rowsPerPage),
    totalCourses: state => state.totalCourses
  }
}
