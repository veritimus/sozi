import UserService from '@/services/UserService'

export default {
  namespaced: true,
  state: {
    lessons: []
  },
  mutations: {
    clearLessons (state) {
      state.lessons = []
    },
    setLessons (state, lessons) {
      state.lessons = lessons
    }
  },
  actions: {
    fetchLessons ({commit, rootState}, queryData) {
      return new Promise(async (resolve, reject) => {
        try {
          const {data} = await UserService.fetchLessonsBetweenDates(rootState.auth.userId, queryData)
          commit('setLessons', data.lessons)
          resolve({lessons: data.lessons})
        } catch (error) {
          reject(error.response.data.error)
        }
      })
    }
  },
  getters: {
    lessons: state => state.lessons
  }
}
