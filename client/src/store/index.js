import Vue from 'vue'
import Vuex from 'vuex'
import auth from '@/store/modules/auth'
import children from '@/store/modules/children'
import educationalUnits from '@/store/modules/educationalUnits'
import lessons from '@/store/modules/lessons'
import locale from '@/store/modules/locale'
import students from '@/store/modules/students'
import user from '@/store/modules/user'

Vue.config.devtools = true
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    children,
    educationalUnits,
    lessons,
    locale,
    students,
    user
  },
  state: {
  },
  mutations: {
  },
  actions: {
  },
  getters: {
  }
})
