export const isAlphanumeric = value => /^[a-zA-Z0-9]+$/.test(value.trim())
export const isDecimal = value => /^[0-9]{0,4}\.[0-9]{2}$/.test(value.trim())
export const isEmpty = value => !value || value.length === 0
export const isEqualOrMore = (value1, value2) => +value1 >= value2
export const isEqualOrLess = (value1, value2) => +value1 <= value2
export const isNotEmpty = value => value.length > 0
export const isNumeric = value => /^[0-9]+$/.test(value.trim())
export const isTime = value => /^(20|21|22|23|[0-1][0-9])[0-5][0-9]$/.test(value.trim())
export const isUrl = value => /^(http|https):\/\/[a-z0-9-.]+\.[^ "]+$/.test(value.trim())
export const minLength = (value, min) => value.trim().length >= min
export const maxLength = (value, max) => value.trim().length <= max
export const required = value => !!value
export const validCity = value => /^(([^\u0000-\u007F]|[^\d]){1}([\s'-]){0,1})+$/i.test(value.trim())
export const validConfPassword = (value, password) => value === password
export const validEmail = value => /.+@.+/.test(value.trim())
export const validFacebook = value => /^(http|https):\/\/[a-z0-9-.]+\.com\/[^ "]+$/.test(value.trim())
export const validFileSize = (value, max) => value <= max
export const validFileType = (value, types) => types.includes(value)
export const validFirstName = value => /^(([^\u0000-\u007F]|[^\d]){1}([\s'-]){0,1})+$/i.test(value.trim())
export const validLastName = value => /^(([^\u0000-\u007F]|[^\d]){1}([\s'-]){0,1})+$/i.test(value.trim())
export const validPassword = value => /^[a-zA-Z0-9#$%?!@]+$/.test(value.trim())
