const resetFormData = formData => {
  Object.keys(formData).forEach(key => {
    if (Array.isArray(formData[key])) {
      formData[key] = []
    } else if (formData[key] && typeof formData[key] !== 'string' && Object.keys(formData[key]).length > 0) {
      resetFormData(formData[key])
    } else if (!key.startsWith('date')) {
      formData[key] = ''
    } else {
      formData[key] = new Date().toISOString().substr(0, 10)
    }
  })
}

export const responses = {
  data () {
    return {
      message: null,
      messageDisplay: false,
      messageType: null
    }
  }
}

export const reset = {
  methods: {
    reset () {
      if (this.$refs.form) {
        this.$refs.form.resetValidation()
      }
      resetFormData(this.formData)
    }
  }
}

export const clearResponseMessage = {
  methods: {
    clearResponseMessage () {
      this.messageDisplay = false
      this.message = null
      this.messageType = null
    }
  }
}

export const delayedClearResponseMessage = {
  methods: {
    clearResponseMessage () {
      setTimeout(() => {
        this.messageDisplay = false
        this.message = null
        this.messageType = null
      }, 1500)
    }
  }
}

export const setResponseMessage = {
  methods: {
    setResponseMessage (message, messageType) {
      this.message = message
      this.messageType = messageType
      this.messageDisplay = true
    }
  }
}

export const setFileError = {
  methods: {
    setFileError (error) {
      this.fileError = error
    }
  }
}

export const setFile = {
  methods: {
    setFile (file) {
      this.formData.file = file
    }
  }
}
