import {mapGetters} from 'vuex'

export const managerStudents = {
  computed: {
    ...mapGetters({
      students: 'students/managerStudents/students',
      pageStudents: 'students/managerStudents/pageStudents',
      paginationLength: 'students/managerStudents/paginationLength',
      totalStudents: 'students/managerStudents/totalStudents',
      units: 'students/managerStudents/units'
    })
  }
}

export const searchStudents = {
  computed: {
    ...mapGetters({
      students: 'students/searchStudents/students',
      paginationLength: 'students/searchStudents/paginationLength',
      totalStudents: 'students/searchStudents/totalStudents'
    })
  }
}

export const teacherStudents = {
  computed: {
    ...mapGetters({
      students: 'students/teacherStudents/students',
      pageStudents: 'students/teacherStudents/pageStudents',
      paginationLength: 'students/teacherStudents/paginationLength',
      totalStudents: 'students/teacherStudents/totalStudents',
      units: 'students/teacherStudents/units'
    })
  }
}
