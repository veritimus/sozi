import * as V from '@/validations'

export const address = {
  data () {
    return {
      addressRules: [v => V.isEmpty(v) || V.maxLength(v, 255) || this.$t('forms.basic.rules.maxLength', {value: 255})]
    }
  }
}

export const bankAccount = {
  data () {
    return {
      bankAccountRules: [
        v => V.isEmpty(v) || V.isNumeric(v) || this.$t('forms.basic.rules.validBankAccount'),
        v => V.isEmpty(v) || V.minLength(v, 26) || this.$t('forms.basic.rules.minLength', {value: 26}),
        v => V.isEmpty(v) || V.maxLength(v, 26) || this.$t('forms.basic.rules.maxLength', {value: 26})
      ]
    }
  }
}

export const courseDescription = {
  data () {
    return {
      courseDescriptionRules: [
        v => V.required(v) || this.$t('forms.basic.rules.required'),
        v => V.isEmpty(v) || V.minLength(v, 5) || this.$t('forms.basic.rules.minLength', {value: 5}),
        v => V.isEmpty(v) || V.maxLength(v, 1000) || this.$t('forms.basic.rules.maxLength', {value: 1000})
      ]
    }
  }
}

export const courseTitle = {
  data () {
    return {
      courseTitleRules: [
        v => V.required(v) || this.$t('forms.basic.rules.required'),
        v => V.isEmpty(v) || V.minLength(v, 5) || this.$t('forms.basic.rules.minLength', {value: 5}),
        v => V.isEmpty(v) || V.maxLength(v, 255) || this.$t('forms.basic.rules.maxLength', {value: 255})
      ]
    }
  }
}

export const file = {
  methods: {
    validSize (size) {
      if (!V.validFileSize(size, this.maxSize)) {
        this.$emit('fileError', {file: this.$t('messages.errors.invalidImageSize', {size: `${this.maxSize / 1024}MB`})})
        return false
      }
      return true
    },
    validType (type) {
      if (!V.validFileType(type, this.fileTypes)) {
        this.$emit('fileError', {file: this.$t('messages.errors.invalidImageType')})
        return false
      }
      return true
    }
  }
}

export const city = {
  data () {
    return {
      cityRules: [
        v => V.required(v) || this.$t('forms.basic.rules.required'),
        v => V.isEmpty(v) || V.maxLength(v, 60) || this.$t('forms.basic.rules.maxLength', {value: 60}),
        v => V.isEmpty(v) || V.validCity(v) || this.$t('forms.basic.rules.validCity'),
        v => V.isEmpty(v) || V.minLength(v, 2) || this.$t('forms.basic.rules.minLength', {value: 2})
      ]
    }
  }
}

export const childUsername = {
  data () {
    return {
      childUsernameRules: [
        v => V.isEmpty(v) || V.isAlphanumeric(v) || this.$t('forms.basic.rules.validUsername'),
        v => V.isEmpty(v) || V.minLength(v, 8) || this.$t('forms.basic.rules.minLength', {value: 8}),
        v => V.isEmpty(v) || V.maxLength(v, 255) || this.$t('forms.basic.rules.maxLength', {value: 255})
      ]
    }
  }
}

export const description = {
  data () {
    return {
      descriptionRules: [v => V.isEmpty(v) || V.maxLength(v, 1000) || this.$t('forms.basic.rules.maxLength', {value: 1000})]
    }
  }
}

export const email = {
  data () {
    return {
      emailRules: [
        v => V.required(v) || this.$t('forms.basic.rules.required'),
        v => V.isEmpty(v) || V.validEmail(v) || this.$t('forms.basic.rules.validEmail'),
        v => V.isEmpty(v) || V.minLength(v, 8) || this.$t('forms.basic.rules.minLength', {value: 8}),
        v => V.isEmpty(v) || V.maxLength(v, 255) || this.$t('forms.basic.rules.maxLength', {value: 255})
      ]
    }
  }
}

export const facebook = {
  data () {
    return {
      facebookRules: [
        v => V.isEmpty(v) || V.isUrl(v) || this.$t('forms.basic.rules.validUrl'),
        v => V.isEmpty(v) || V.validFacebook(v) || this.$t('forms.basic.rules.validFacebook'),
        v => V.isEmpty(v) || V.minLength(v, 15) || this.$t('forms.basic.rules.minLength', {value: 15}),
        v => V.isEmpty(v) || V.maxLength(v, 255) || this.$t('forms.basic.rules.maxLength', {value: 255})
      ]
    }
  }
}

export const firstName = {
  data () {
    return {
      firstNameRules: [
        v => V.required(v) || this.$t('forms.basic.rules.required'),
        v => V.isEmpty(v) || V.validFirstName(v) || this.$t('forms.basic.rules.validFirstName'),
        v => V.isEmpty(v) || V.minLength(v, 2) || this.$t('forms.basic.rules.minLength', {value: 2}),
        v => V.isEmpty(v) || V.maxLength(v, 255) || this.$t('forms.basic.rules.maxLength', {value: 255})
      ]
    }
  }
}

export const newPassword = {
  data () {
    return {
      newPasswordRules: [
        v => V.isEmpty(v) || V.minLength(v, 8) || this.$t('forms.basic.rules.minLength', {value: 8}),
        v => V.isEmpty(v) || V.maxLength(v, 245) || this.$t('forms.basic.rules.maxLength', {value: 245}),
        v => V.isEmpty(v) || V.validPassword(v) || this.$t('forms.basic.rules.validPassword')
      ]
    }
  }
}

export const newPasswordConf = {
  data () {
    return {
      newPasswordConfRules: [
        v => V.isEmpty(v) || V.validConfPassword(v, this.formData.password) || this.$t('forms.basic.rules.validConfPassword')
      ]
    }
  }
}

export const lastName = {
  data () {
    return {
      lastNameRules: [
        v => V.required(v) || this.$t('forms.basic.rules.required'),
        v => V.isEmpty(v) || V.validLastName(v) || this.$t('forms.basic.rules.validLastName'),
        v => V.isEmpty(v) || V.minLength(v, 2) || this.$t('forms.basic.rules.minLength', {value: 2}),
        v => V.isEmpty(v) || V.maxLength(v, 255) || this.$t('forms.basic.rules.maxLength', {value: 255})
      ]
    }
  }
}

export const lessonCourse = {
  data () {
    return {
      lessonCourseRules: [v => V.required(v) || this.$t('forms.basic.rules.required')]
    }
  }
}

export const lessonDays = {
  data () {
    return {
      lessonDaysRules: [v => V.isNotEmpty(v) || this.$t('forms.basic.rules.validLessonDays')]
    }
  }
}

export const lessonDuration = {
  data () {
    return {
      lessonDurationRules: [
        v => V.required(v) || this.$t('forms.basic.rules.required'),
        v => V.isEqualOrMore(v, 0) || this.$t('forms.basic.rules.equalOrMore', {value: 0}),
        v => V.isEqualOrLess(v, 1000) || this.$t('forms.basic.rules.equalOrLess', {value: 1000})
      ]
    }
  }
}

export const lessonHomework = {
  data () {
    return {
      lessonHomeworkRules: [v => V.isEmpty(v) || V.maxLength(v, 1000) || this.$t('forms.basic.rules.maxLength', {value: 1000})]
    }
  }
}

export const lessonMessageForParent = {
  data () {
    return {
      lessonMessageForParentRules: [v => V.isEmpty(v) || V.maxLength(v, 1000) || this.$t('forms.basic.rules.maxLength', {value: 1000})]
    }
  }
}

export const lessonMessageForTeacher = {
  data () {
    return {
      lessonMessageForTeacherRules: [v => V.isEmpty(v) || V.maxLength(v, 1000) || this.$t('forms.basic.rules.maxLength', {value: 1000})]
    }
  }
}

export const lessonQuantity = {
  data () {
    return {
      lessonQuantityRules: [
        v => V.isEqualOrMore(v, 0) || this.$t('forms.basic.rules.equalOrMore', {value: 0}),
        v => V.isEqualOrLess(v, 1000) || this.$t('forms.basic.rules.equalOrLess', {value: 1000})
      ]
    }
  }
}

export const lessonPrice = {
  data () {
    return {
      lessonPriceRules: [
        v => V.required(v) || this.$t('forms.basic.rules.required'),
        v => V.isEmpty(v) || V.isDecimal(v) || this.$t('forms.basic.rules.validLessonPrice'),
        v => V.isEmpty(v) || V.isEqualOrMore(v, 0.00) || this.$t('forms.basic.rules.equalOrMore', {value: 0.00}),
        v => V.isEmpty(v) || V.isEqualOrLess(v, 9999.99) || this.$t('forms.basic.rules.equalOrLess', {value: 9999.99})
      ]
    }
  }
}

export const lessonStudent = {
  data () {
    return {
      lessonStudentRules: [v => V.required(v) || this.$t('forms.basic.rules.required')]
    }
  }
}

export const lessonTeacherNotes = {
  data () {
    return {
      lessonTeacherNotesRules: [v => V.isEmpty(v) || V.maxLength(v, 1000) || this.$t('forms.basic.rules.maxLength', {value: 1000})]
    }
  }
}

export const lessonTime = {
  data () {
    return {
      lessonTimeRules: [
        v => V.required(v) || this.$t('forms.basic.rules.required'),
        v => V.isEmpty(v) || V.isTime(v) || this.$t('forms.basic.rules.validLessonTime')
      ]
    }
  }
}

export const password = {
  data () {
    return {
      passwordRules: [
        v => V.required(v) || this.$t('forms.basic.rules.required'),
        v => V.minLength(v, 8) || this.$t('forms.basic.rules.minLength', {value: 8}),
        v => V.maxLength(v, 245) || this.$t('forms.basic.rules.maxLength', {value: 245}),
        v => V.validPassword(v) || this.$t('forms.basic.rules.validPassword')
      ]
    }
  }
}

export const passwordConf = {
  data () {
    return {
      passwordConfRules: [
        v => V.required(v) || this.$t('forms.basic.rules.required'),
        v => V.validConfPassword(v, this.formData.password) || this.$t('forms.basic.rules.validConfPassword')
      ]
    }
  }
}

export const passwordConfAcc = {
  data () {
    return {
      passwordConfRules: [
        v => V.required(v) || this.$t('forms.basic.rules.required'),
        v => V.validConfPassword(v, this.accFormData.password) || this.$t('forms.basic.rules.validConfPassword')
      ]
    }
  }
}

export const phone = {
  data () {
    return {
      phoneRules: [
        v => V.isEmpty(v) || V.isNumeric(v) || this.$t('forms.basic.rules.validPhone'),
        v => V.isEmpty(v) || V.minLength(v, 9) || this.$t('forms.basic.rules.minLength', {value: 9}),
        v => V.isEmpty(v) || V.maxLength(v, 15) || this.$t('forms.basic.rules.maxLength', {value: 15})
      ]
    }
  }
}

export const region = {
  data () {
    return {
      regionRules: [v => V.required(v) || this.$t('forms.basic.rules.required')]
    }
  }
}

export const role = {
  data () {
    return {
      roleRules: [v => V.isNotEmpty(v) || this.$t('forms.basic.rules.validRole')]
    }
  }
}

export const subjectName = {
  data () {
    return {
      subjectNameRules: [
        v => V.isEmpty(v) || V.minLength(v, 2) || this.$t('forms.basic.rules.minLength', {value: 2}),
        v => V.isEmpty(v) || V.maxLength(v, 60) || this.$t('forms.basic.rules.maxLength', {value: 60})
      ]
    }
  }
}

export const unitName = {
  data () {
    return {
      unitNameRules: [
        v => V.required(v) || this.$t('forms.basic.rules.required'),
        v => V.isEmpty(v) || V.minLength(v, 3) || this.$t('forms.basic.rules.minLength', {value: 3}),
        v => V.isEmpty(v) || V.maxLength(v, 255) || this.$t('forms.basic.rules.maxLength', {value: 255})
      ]
    }
  }
}

export const unitType = {
  data () {
    return {
      unitTypeRules: [v => V.required(v) || this.$t('forms.basic.rules.required')]
    }
  }
}

export const username = {
  data () {
    return {
      usernameRules: [
        v => V.required(v) || this.$t('forms.basic.rules.required'),
        v => V.isEmpty(v) || V.isAlphanumeric(v) || this.$t('forms.basic.rules.validUsername'),
        v => V.isEmpty(v) || V.minLength(v, 8) || this.$t('forms.basic.rules.minLength', {value: 8}),
        v => V.isEmpty(v) || V.maxLength(v, 255) || this.$t('forms.basic.rules.maxLength', {value: 255})
      ]
    }
  }
}

export const website = {
  data () {
    return {
      websiteRules: [
        v => V.isEmpty(v) || V.isUrl(v) || this.$t('forms.basic.rules.validUrl'),
        v => V.isEmpty(v) || V.minLength(v, 12) || this.$t('forms.basic.rules.minLength', {value: 12}),
        v => V.isEmpty(v) || V.maxLength(v, 255) || this.$t('forms.basic.rules.maxLength', {value: 255})
      ]
    }
  }
}
