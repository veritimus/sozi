import {mapGetters} from 'vuex'

export const children = {
  computed: {
    ...mapGetters({
      children: 'children/children',
      pageChildren: 'children/pageChildren',
      paginationLength: 'children/paginationLength',
      totalChildren: 'children/totalChildren'
    })
  }
}
