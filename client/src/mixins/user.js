import {mapGetters} from 'vuex'

export const filterMenuItems = {
  computed: {
    filteredMenuItems () {
      return this.menuItems.filter(item => this.filterMenuItems(item.roleRightsLvl))
    }
  },
  methods: {
    filterMenuItems (role) {
      return role === 'all' ||
        this.roles.includes(role) ||
        (role === 'ATM' && (this.roles.includes('teacher') || this.roles.includes('manager') || this.roles.includes('admin'))) ||
        (role === 'allExceptStudents' && !this.roles.includes('student'))
    }
  }
}

export const filterTabItems = {
  methods: {
    filterTabItems (role) {
      return role === 'all' ||
        this.roles.includes(role) ||
        (role === 'teacherOrManager' && (this.roles.includes('teacher') || this.roles.includes('manager'))) ||
        (role === 'studentOrParent' && (this.roles.includes('student') || this.roles.includes('parent')))
    }
  }
}

export const isAuthenticated = {
  computed: {
    ...mapGetters({
      isAuthenticated: 'auth/isAuthenticated'
    })
  }
}

export const profileIcon = {
  computed: {
    profileIcon () {
      if (this.roles.includes('teacher') || this.roles.includes('manager')) {
        return '-tie'
      } else if (this.roles.includes('admin')) {
        return '-secret'
      } else {
        if (this.roles.includes('parent')) {
          return ''
        } else {
          return '-graduate'
        }
      }
    }
  }
}

export const dataProperties = {
  methods: {
    formatData (data, type) {
      if (typeof data === 'string' || typeof data === 'number') {
        return data
      } else if (!data) {
        return '-----'
      } else {
        if (type === 'days') {
          return data.map(day => this.$t(`days.${day}`)).join(', ')
        } else {
          return data.join(', ')
        }
      }
    },
    isPropertyNested (property) {
      if (!property.title.includes('.')) {
        return this.formatData(this.item[property.title], property.type)
      } else {
        return this.formatData(this.item[property.title.substring(0, property.title.indexOf('.'))][property.title.substring(property.title.indexOf('.') + 1)], property.type)
      }
    }
  }
}

export const user = {
  computed: {
    ...mapGetters({
      user: 'user/user'
    })
  }
}

export const userId = {
  computed: {
    ...mapGetters({
      userId: 'auth/userId'
    })
  }
}

export const roles = {
  computed: {
    ...mapGetters({
      roles: 'user/roles'
    })
  }
}

export const translatedRoles = {
  computed: {
    translatedRoles () {
      const translatedRoles = []
      this.roles.forEach(role => {
        for (let i = 0; i < 4; i++) {
          if (role === this.$t(`roles[${i}].value`)) {
            translatedRoles.push(this.$t(`roles[${i}].name`))
          }
        }
      })
      return translatedRoles
    }
  }
}
