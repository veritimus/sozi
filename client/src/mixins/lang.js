import {mapGetters} from 'vuex'

export const lang = {
  computed: {
    ...mapGetters({
      lang: 'locale/lang'
    })
  }
}
