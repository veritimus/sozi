import {mapActions} from 'vuex'

export const fetchChildren = {
  async created () {
    if (!this.children) {
      await this.fetchChildren()
    }
    this.paginatedChildren = this.pageChildren(1, this.rowsQuantity)
  },
  methods: {
    ...mapActions({
      fetchChildren: 'children/fetchChildren'
    })
  }
}

export const fetchChildrenLessons = {
  methods: {
    ...mapActions({
      fetchLessons: 'lessons/childrenLessons/fetchLessons'
    })
  }
}

export const fetchChildrenUnits = {
  async created () {
    if (this.children.length > 0) {
      this.child = this.children[0]
    }
    this.paginatedUnits = this.pageUnits(1, this.rowsQuantity)
  },
  methods: {
    ...mapActions({
      changeStatus: 'educationalUnits/childrenUnits/changeStatus',
      fetchUnits: 'educationalUnits/childrenUnits/fetchUnits'
    })
  }
}

export const fetchManagerUnits = {
  async created () {
    await this.fetchUnits({page: 1, rows: 6})
  },
  methods: {
    ...mapActions({
      fetchUnits: 'educationalUnits/managerUnits/fetchUnits'
    })
  }
}

export const fetchMyLessons = {
  methods: {
    ...mapActions({
      fetchLessons: 'lessons/myLessons/fetchLessons'
    })
  }
}

export const fetchMyUnits = {
  async created () {
    await this.fetchUnits()
    this.paginatedUnits = this.pageUnits(1, this.rowsQuantity)
  },
  methods: {
    ...mapActions({
      changeStatus: 'educationalUnits/myUnits/changeStatus',
      fetchUnits: 'educationalUnits/myUnits/fetchUnits'
    })
  }
}

export const fetchLessons = {
  async created () {
    await this.fetchLessons({
      startDate: this.$moment().subtract(1, 'months').hours(0).minutes(0).seconds(0).toISOString(),
      endDate: this.$moment().add(1, 'months').hours(23).minutes(59).seconds(59).toISOString(),
      status: 'all'
    })
  },
  methods: {
    async updateLessons (newDate) {
      await this.fetchLessons({
        startDate: this.$moment(newDate).subtract(1, 'months').hours(0).minutes(0).seconds(0).toISOString(),
        endDate: this.$moment(newDate).add(1, 'months').hours(23).minutes(59).seconds(59).toISOString(),
        status: this.lessonsStatus
      })
    }
  }
}

export const fetchSearchCourses = {
  async created () {
    this.formData.city = this.user.city
    this.fetch({page: 1, rows: this.rowsQuantity})
  },
  methods: {
    ...mapActions({
      fetchCourses: 'lessons/searchCourses/fetchCourses'
    })
  }
}

export const fetchSearchStudents = {
  async created () {
    this.formData.city = this.user.city
    await this.fetch({page: 1, rows: this.rowsQuantity})
  },
  methods: {
    ...mapActions({
      fetchStudents: 'students/searchStudents/fetchStudents'
    })
  }
}

export const fetchSearchUnits = {
  async created () {
    this.formData.city = this.user.city
    await this.fetch({page: 1, rows: this.rowsQuantity})
  },
  methods: {
    ...mapActions({
      fetchUnits: 'educationalUnits/searchUnits/fetchUnits'
    })
  }
}

export const fetchTeacherLessons = {
  methods: {
    ...mapActions({
      createLesson: 'lessons/teacherLessons/createLesson',
      fetchCourses: 'lessons/teacherLessons/fetchCourses',
      fetchLessons: 'lessons/teacherLessons/fetchLessons',
      setStudents: 'lessons/teacherLessons/setStudents'
    })
  }
}

export const fetchTeacherUnits = {
  async created () {
    await this.fetchUnits()
    this.paginatedUnits = this.pageUnits(1, this.rowsQuantity)
  },
  methods: {
    ...mapActions({
      changeStatus: 'educationalUnits/teacherUnits/changeStatus',
      fetchUnits: 'educationalUnits/teacherUnits/fetchUnits'
    })
  }
}

export const fetchUnitCourses = {
  async created () {
    await this.fetchUnits()
    this.unit = this.units[0]
  },
  methods: {
    ...mapActions({
      changeStatus: 'lessons/myCourses/changeStatus',
      fetchCourses: 'lessons/myCourses/fetchCourses',
      fetchUnits: 'lessons/myCourses/fetchUnits'
    })
  }
}

export const fetchUnitLessons = {
  async created () {
    await this.fetchUnits()
    if (this.units.length > 0) {
      this.unit = this.units[0]
    }
  },
  methods: {
    ...mapActions({
      createLesson: 'lessons/unitLessonsSchedule/createLesson',
      fetchCourses: 'lessons/unitLessonsSchedule/fetchCourses',
      fetchLessons: 'lessons/unitLessonsSchedule/fetchLessons',
      fetchUnits: 'lessons/unitLessonsSchedule/fetchUnits',
      setStudents: 'lessons/unitLessonsSchedule/setStudents'
    })
  }
}

export const fetchUnitStudents = {
  async created () {
    await this.fetchUnits()
    this.unit = this.units[0]
  },
  methods: {
    ...mapActions({
      changeStatus: 'students/managerStudents/changeStatus',
      fetchStudents: 'students/managerStudents/fetchStudents',
      fetchUnits: 'students/managerStudents/fetchUnits'
    })
  }
}

export const fetchUnitTeacherStudents = {
  async created () {
    await this.fetchUnits()
    this.unit = this.units[0]
  },
  methods: {
    ...mapActions({
      fetchStudents: 'students/teacherStudents/fetchStudents',
      fetchUnits: 'students/teacherStudents/fetchUnits'
    })
  }
}
