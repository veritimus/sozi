export const childItem = {
  data () {
    return {
      childItemIcon: 'child',
      childItemProperties: {
        alphaProperties: [
          {title: 'region', icon: 'atlas'},
          {title: 'city', icon: 'city'},
          {title: 'email', icon: 'envelope'},
          {title: 'phone', icon: 'phone'}
        ],
        dateProperties: [
          {title: 'birthday', icon: 'birthday-cake'}
        ]
      }
    }
  }
}

export const courseItem = {
  data () {
    return {
      courseItemIcon: 'chalkboard-teacher',
      courseItemProperties: {
        alphaProperties: [
          {title: 'EducationalUnit.name', icon: 'university'},
          {title: 'defaultDuration', unity: 'minutes', icon: 'stopwatch'},
          {title: 'lessonsQuantity', unity: 'lessons', icon: 'chalkboard'},
          {title: 'defaultDay', type: 'days', icon: 'calendar-alt'},
          {title: 'defaultTime', icon: 'clock'},
          {title: 'defaultPrice', unity: 'zł', icon: 'dollar-sign'}
        ]
      }
    }
  }
}

export const courseItemExtended = {
  data () {
    return {
      courseItemIcon: 'chalkboard-teacher',
      courseItemProperties: {
        alphaProperties: [
          {title: 'EducationalUnit.name', icon: 'university'},
          {title: 'EducationalUnit.region', icon: 'atlas'},
          {title: 'EducationalUnit.city', icon: 'city'},
          {title: 'defaultDuration', unity: 'minutes', icon: 'stopwatch'},
          {title: 'lessonsQuantity', unity: 'lessons', icon: 'chalkboard'},
          {title: 'defaultDay', type: 'days', icon: 'calendar-alt'},
          {title: 'defaultTime', icon: 'clock'},
          {title: 'defaultPrice', unity: 'zł', icon: 'dollar-sign'}
        ]
      }
    }
  }
}

export const eduUnitItem = {
  data () {
    return {
      eduUnitItemIcon: 'university',
      eduUnitItemProperties: {
        alphaProperties: [
          {title: 'region', icon: 'atlas'},
          {title: 'city', icon: 'city'},
          {title: 'address', icon: 'map-signs'},
          {title: 'email', icon: 'envelope'},
          {title: 'phone', icon: 'phone'},
          {title: 'website', icon: 'globe'}
        ],
        dateProperties: []
      }
    }
  }
}

export const eduUnitItemExtended = {
  data () {
    return {
      eduUnitItemIcon: 'university',
      eduUnitItemProperties: {
        addressProperties: [
          {title: 'region', icon: 'atlas'},
          {title: 'city', icon: 'city'},
          {title: 'address', icon: 'map-signs'}
        ],
        contactProperties: [
          {title: 'email', icon: 'fas fa-envelope'},
          {title: 'phone', icon: 'fas fa-phone'},
          {title: 'website', icon: 'fas fa-globe'},
          {title: 'facebook', icon: 'fab fa-facebook'}
        ],
        generalProperties: [
          {title: 'teachers', unity: 'teachers', icon: 'user-tie'},
          {title: 'Courses', unity: 'courses', icon: 'chalkboard-teacher'},
          {title: 'students', unity: 'students', icon: 'user-graduate'}
        ]
      }
    }
  }
}

export const studentItem = {
  data () {
    return {
      studentItemIcon: 'user-graduate',
      studentItemProperties: {
        alphaProperties: [
          {title: 'region', icon: 'atlas'},
          {title: 'city', icon: 'city'},
          {title: 'email', icon: 'envelope'},
          {title: 'phone', icon: 'phone'}
        ],
        dateProperties: [
          {title: 'birthday', icon: 'birthday-cake'}
        ]
      }
    }
  }
}

export const teacherItem = {
  data () {
    return {
      teacherItemIcon: 'user-tie',
      teacherItemProperties: {
        alphaProperties: [
          {title: 'region', icon: 'atlas'},
          {title: 'city', icon: 'city'},
          {title: 'email', icon: 'envelope'},
          {title: 'phone', icon: 'phone'}
        ],
        dateProperties: [
          {title: 'birthday', icon: 'birthday-cake'}
        ]
      }
    }
  }
}

export const userItem = {
  data () {
    return {
      userItemIcon: 'user',
      userItemProperties: {
        alphaProperties: [
          {title: 'region', icon: 'atlas'},
          {title: 'city', icon: 'city'},
          {title: 'email', icon: 'envelope'},
          {title: 'phone', icon: 'phone'}
        ],
        dateProperties: [
          {title: 'birthday', icon: 'birthday-cake'}
        ]
      }
    }
  }
}

export const userItemExtended = {
  data () {
    return {
      userItemIcon: 'user',
      userItemProperties: {
        addressProperties: [
          {title: 'region', icon: 'atlas'},
          {title: 'city', icon: 'city'}
        ],
        contactProperties: [
          {title: 'email', icon: 'fas fa-envelope'},
          {title: 'phone', icon: 'fas fa-phone'}
        ],
        dateProperties: [
          {title: 'birthday', icon: 'birthday-cake'},
          {title: 'registrationDate', icon: 'user-plus'}
        ]
      }
    }
  }
}
