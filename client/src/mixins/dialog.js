export const displayDialogSetup = {
  data () {
    return {
      state: false
    }
  },
  methods: {
    dialogState (state) {
      this.state = state
    },
    changeStatus (event) {
      this.dialogState(event.dialogState)
      this.$emit('changeItemStatus', event.status)
    }
  }
}
