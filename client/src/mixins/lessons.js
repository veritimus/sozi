import {mapGetters} from 'vuex'

export const lessons = {
  data () {
    return {
      statusTypes: [
        {name: this.$t('views.Lessons.lessonsStatusTypes[0]'), value: 'all'},
        {name: this.$t('views.Lessons.lessonsStatusTypes[1]'), value: 'active'},
        {name: this.$t('views.Lessons.lessonsStatusTypes[2]'), value: 'inactive'}
      ],
      lessonsStatus: 'all'
    }
  },
  watch: {
    async lessonsStatus () {
      await this.fetchLessons({
        startDate: this.$moment().hours(0).minutes(0).seconds(0).toISOString(),
        endDate: this.$moment().add(7, 'days').hours(23).minutes(59).seconds(59).toISOString(),
        status: this.lessonsStatus
      })
    }
  }
}

export const myCourses = {
  computed: {
    ...mapGetters({
      courses: 'lessons/myCourses/courses',
      pageCourses: 'lessons/myCourses/pageCourses',
      paginationLength: 'lessons/myCourses/paginationLength',
      totalCourses: 'lessons/myCourses/totalCourses',
      units: 'lessons/myCourses/units'
    })
  }
}

export const childrenLessons = {
  computed: {
    ...mapGetters({
      lessons: 'lessons/childrenLessons/lessons'
    })
  }
}

export const myLessons = {
  computed: {
    ...mapGetters({
      lessons: 'lessons/myLessons/lessons'
    })
  }
}

export const teacherLessons = {
  computed: {
    ...mapGetters({
      courses: 'lessons/teacherLessons/courses',
      lessons: 'lessons/teacherLessons/lessons',
      students: 'lessons/teacherLessons/students',
      units: 'lessons/teacherLessons/units'
    })
  }
}

export const unitLessonsSchedule = {
  computed: {
    ...mapGetters({
      courses: 'lessons/unitLessonsSchedule/courses',
      lessons: 'lessons/unitLessonsSchedule/lessons',
      students: 'lessons/unitLessonsSchedule/students',
      units: 'lessons/unitLessonsSchedule/units'
    })
  }
}

export const searchCourses = {
  computed: {
    ...mapGetters({
      courses: 'lessons/searchCourses/courses',
      paginationLength: 'lessons/searchCourses/paginationLength',
      totalCourses: 'lessons/searchCourses/totalCourses'
    })
  }
}
