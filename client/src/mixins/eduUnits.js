import {mapGetters} from 'vuex'

export const userAcceptance = {
  methods: {
    async decision (status, itemId, itemType) {
      try {
        const {message, type} = await this.changeStatus({itemId, unitId: this.unitId, itemType, status})
        this.setResponseMessage(message, type)
        this.clearResponseMessage()
      } catch (error) {
        this.setResponseMessage(error, 'error')
        this.clearResponseMessage()
      }
    }
  }
}

export const eduUnitTypes = {
  data () {
    return {
      unitTypes: {
        individual: true,
        school: true
      }
    }
  },
  computed: {
    availableEduUnitTypes () {
      if (this.$i18n.messages[this.lang]) {
        return this.$i18n.messages[this.lang].views.EducationalUnits.eduUnitTypes
      }
    }
  },
  methods: {
    setUnitType () {
      Object.keys(this.unitTypes).forEach(type => {
        if (this.unitTypes[type] && !this.formData.types.includes(type)) {
          this.formData.types.push(type)
        } else if (!this.unitTypes[type] && this.formData.types.includes(type)) {
          this.formData.types.splice(this.formData.types.indexOf(type), 1)
        }
      })
    }
  }
}

export const childrenUnits = {
  computed: {
    ...mapGetters({
      units: 'educationalUnits/childrenUnits/units',
      pageUnits: 'educationalUnits/childrenUnits/pageUnits',
      paginationLength: 'educationalUnits/childrenUnits/paginationLength',
      totalUnits: 'educationalUnits/childrenUnits/totalUnits'
    })
  }
}

export const managerUnits = {
  computed: {
    ...mapGetters({
      units: 'educationalUnits/managerUnits/units',
      paginationLength: 'educationalUnits/managerUnits/paginationLength',
      unitsIdListActiveItems: 'educationalUnits/managerUnits/unitsIdListActiveItems',
      unitsIdListPendingItems: 'educationalUnits/managerUnits/unitsIdListPendingItems',
      unitsItemsActive: 'educationalUnits/managerUnits/unitsItemsActive',
      unitsItemsPending: 'educationalUnits/managerUnits/unitsItemsPending',
      totalUnits: 'educationalUnits/managerUnits/totalUnits'
    })
  }
}

export const myUnits = {
  computed: {
    ...mapGetters({
      units: 'educationalUnits/myUnits/units',
      pageUnits: 'educationalUnits/myUnits/pageUnits',
      paginationLength: 'educationalUnits/myUnits/paginationLength',
      totalUnits: 'educationalUnits/myUnits/totalUnits'
    })
  }
}

export const teacherUnits = {
  computed: {
    ...mapGetters({
      units: 'educationalUnits/teacherUnits/units',
      pageUnits: 'educationalUnits/teacherUnits/pageUnits',
      paginationLength: 'educationalUnits/teacherUnits/paginationLength',
      totalUnits: 'educationalUnits/teacherUnits/totalUnits'
    })
  }
}

export const searchUnits = {
  computed: {
    ...mapGetters({
      units: 'educationalUnits/searchUnits/units',
      totalUnits: 'educationalUnits/searchUnits/totalUnits'
    })
  }
}
