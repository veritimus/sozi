export const date = {
  methods: {
    formatDate (date) {
      return date ? this.$moment(date).format('DD.MM.YYYY') : '-----'
    }
  }
}

export const datePicker = {
  data () {
    return {
      datePickerSetup: {
        reactive: true,
        minDate: this.$moment().toISOString(),
        minPaymentDate: this.$moment().add(7, 'days').toISOString(),
        maxDate: this.$moment().toISOString(),
        menu: false,
        menu2: false
      }
    }
  }
}
