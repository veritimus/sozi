export const paginationSetup = {
  data () {
    return {
      page: 1,
      rowsQuantity: 6
    }
  }
}

export const setPage = {
  methods: {
    setPage (page, rows, fetch) {
      this.page = page
      fetch({page, rows})
    }
  }
}
