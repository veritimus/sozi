import Vue from 'vue'
import Router from 'vue-router'
import {availableLanguages, loadLanguageAsync} from '@/i18n.config'
import store from '@/store'
import ViewLangWrapper from '@/views/auth/ViewLangWrapper.vue'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.VUE_APP_BASE_URL,
  routes: [
    {path: '', redirect: `/${store.state.locale.lang}/login`},
    {
      path: '/:lang',
      component: ViewLangWrapper,
      children: [
        {path: 'register', name: 'register', component: () => import('@/views/auth/ViewRegistration.vue')},
        {path: 'login', name: 'login', component: () => import('@/views/auth/ViewLogin.vue')},
        {path: 'verify/:verification_token', name: 'verification', component: () => import('@/views/auth/ViewVerification.vue')},
        {path: 'passwordRecover', name: 'passwordRecover', component: () => import('@/views/auth/ViewPasswordRecover.vue')},
        {path: 'auth/generate_verification_token', name: 'generateVerificationToken', component: () => import('@/views/auth/ViewVerificationTokenGenerator.vue')},
        {path: '404', name: '404', component: () => import('@/views/auth/View404.vue')},
        {
          path: 'user',
          component: () => import('@/views/user/ViewUserWrapper.vue'),
          meta: {auth: true},
          children: [
            {path: 'home', name: 'home', component: () => import('@/views/home/ViewHome.vue')},
            {
              path: 'profile',
              component: () => import('@/views/ViewWrapper.vue'),
              children: [
                {path: '', name: 'profile', component: () => import('@/views/profile/ViewProfile.vue')},
                {path: ':id', name: 'userProfile', component: () => import('@/views/profile/ViewUserProfile.vue')}
              ]
            },
            {path: 'children', name: 'children', component: () => import('@/views/children/ViewChildren.vue')},
            {path: 'students', name: 'students', component: () => import('@/views/students/ViewStudents.vue')},
            {
              path: 'lessons',
              component: () => import('@/views/ViewWrapper.vue'),
              children: [
                {path: '', name: 'lessons', component: () => import('@/views/lessons/ViewLessons.vue')},
                {path: ':id', name: 'lessonProfile', component: () => import('@/views/profile/ViewLessonProfile.vue')}
              ]
            },
            {path: 'courses/:id', name: 'courseProfile', component: () => import('@/views/profile/ViewCourseProfile.vue')},
            {
              path: 'eduUnits',
              component: () => import('@/views/ViewWrapper.vue'),
              children: [
                {path: '', name: 'eduUnits', component: () => import('@/views/eduUnits/ViewEduUnits.vue')},
                {path: ':id', name: 'eduUnitProfile', component: () => import('@/views/profile/ViewEduUnitProfile.vue')}
              ]
            }
          ]
        }
      ]
    },
    {path: '/logout', name: 'logout'},
    {path: '*', redirect: `/${store.state.locale.lang}/404`}
  ]
})

router.beforeEach((to, from, next) => {
  if (to.path === '/logout') {
    store.dispatch('auth/logout').then(() => next(`/${store.state.locale.lang}/login`))
  }
  const {lang} = to.params
  if (availableLanguages.includes(lang)) {
    loadLanguageAsync(lang).then(async () => {
      if (to.matched.some(record => record.meta.auth)) {
        if (!store.state.auth.token && !localStorage.getItem('user-token')) {
          next({path: `/${lang}/login`})
        } else {
          if (!store.state.auth.userId && !store.state.auth.token) {
            await store.dispatch('auth/setAuth', {
              id: parseInt(localStorage.getItem('user-id')),
              token: localStorage.getItem('user-token')
            })
          }
          if (!store.state.user.user) {
            await store.dispatch('user/fetchUser', store.state.auth.userId)
          }
          next()
        }
      } else {
        next()
      }
    })
  } else {
    next({name: '404', params: {lang: store.state.locale.lang}})
  }
})

export default router
