const nodemailer = require('nodemailer')
const {mailerKeys} = require('./')

const transporter = nodemailer.createTransport({
  service: mailerKeys.service,
  auth: {
    user: mailerKeys.user,
    pass: mailerKeys.password
  }
})

module.exports = transporter
