const {domainUrl} = require('./')

module.exports = {
  registration: {
    subject: i18n => i18n.t('mailer.registration.title'),
    html: ({firstName, lastName, validityToken, i18n}) => {
      return `
        <p style="margin: 0">${i18n.t('mailer.welcome', {firstName, lastName})}</p>
        <p style="margin: 0">${i18n.t('mailer.registration.message.accountCreateInfo')}</p>
        <p><a href="${domainUrl}${i18n.language}/verify/${validityToken}">${domainUrl}${i18n.language}/verify/${validityToken}</a></p>
        <p>${i18n.t('mailer.registration.message.ignoreMessageInfo')}</p>
        <p>${i18n.t('mailer.signature')}</p>
      `
    }
  },
  registrationChildAccountParentMessage: {
    subject: i18n => i18n.t('mailer.registrationChildAccountParentMessage.title'),
    html: ({firstName, lastName, childFirstName, childLastName, childUsername, validityToken, i18n}) => {
      return `
        <p style="margin: 0">${i18n.t('mailer.welcome', {firstName, lastName})}</p>
        <p style="margin: 0">${i18n.t('mailer.registrationChildAccountParentMessage.message.accountCreateInfo', {firstName: childFirstName, lastName: childLastName})}</p>
        <p><a href="${domainUrl}${i18n.language}/verify/${validityToken}">${domainUrl}${i18n.language}/verify/${validityToken}</a></p>
        <p>${i18n.t('mailer.registrationChildAccountParentMessage.message.loginCredentialsInfo', {username: childUsername})}</p>
        <p>${i18n.t('mailer.signature')}</p>
      `
    }
  },
  registrationChildAccountChildMessage: {
    subject: i18n => i18n.t('mailer.registrationChildAccountChildMessage.title'),
    html: ({parentFirstName, parentLastName, firstName, lastName, username, validityToken, i18n}) => {
      return `
        <p style="margin: 0">${i18n.t('mailer.welcome', {firstName, lastName})}</p>
        <p style="margin: 0">${i18n.t('mailer.registrationChildAccountChildMessage.message.accountCreateInfo', {firstName: parentFirstName, lastName: parentLastName})}</p>
        <p><a href="${domainUrl}${i18n.language}/verify/${validityToken}">${domainUrl}${i18n.language}/verify/${validityToken}</a></p>
        <p>${i18n.t('mailer.registrationChildAccountChildMessage.message.loginCredentialsInfo', {username})}</p>
        <p>${i18n.t('mailer.signature')}</p>
      `
    }
  },
  sendingChildPassword: {
    subject: i18n => i18n.t('mailer.reqChildNewPassword.title'),
    html: ({firstName, lastName, childFirstName, childLastName, password, i18n}) => {
      return `
        <p style="margin: 0">${i18n.t('mailer.welcome', {firstName, lastName})}</p>
        <p style="margin: 0 0 15px 0">${i18n.t('mailer.reqChildNewPassword.message.newPasswordInfo', {firstName: childFirstName, lastName: childLastName})}</p>
        <p style="margin: 0 0 15px 0">${i18n.t('mailer.reqChildNewPassword.message.newPassword')} ${password}</p>
        <p>${i18n.t('mailer.signature')}</p>
      `
    }
  },
  sendingPassword: {
    subject: i18n => i18n.t('mailer.reqNewPassword.title'),
    html: ({firstName, lastName, password, i18n}) => {
      return `
        <p style="margin: 0">${i18n.t('mailer.welcome', {firstName, lastName})}</p>
        <p style="margin: 0 0 15px 0">${i18n.t('mailer.reqNewPassword.message.newPasswordInfo')}</p>
        <p style="margin: 0 0 15px 0">${i18n.t('mailer.reqNewPassword.message.newPassword')} ${password}</p>
        <p>${i18n.t('mailer.signature')}</p>
      `
    }
  },
  sendingValidityToken: {
    subject: i18n => i18n.t('mailer.reqValidityToken.title'),
    html: ({firstName, lastName, token, i18n}) => {
      return `
        <p style="margin: 0">${i18n.t('mailer.welcome', {firstName, lastName})}</p>
        <p style="margin: 0">${i18n.t('mailer.reqValidityToken.message.tokenGenerationInfo')}</p>
        <p><a href="${domainUrl}${i18n.language}/verify/${token}">${domainUrl}${i18n.language}/verify/${token}</a></p>
        <p>${i18n.t('mailer.reqValidityToken.message.ignoreMessageInfo')}</p>
        <p>${i18n.t('mailer.signature')}</p>
      `
    }
  }
}
