const {DateTime} = require('luxon')

module.exports = async (models, admin) => {
  const parent1 = await models.User.create({
    username: 'johndoe1',
    email: 'johndoe1@sozi.com',
    password: 'qwertyuiop',
    firstName: 'John',
    lastName: 'Doe',
    city: 'Wrocław',
    region: 'dolnośląskie',
    isActive: true,
    phone: '999999999'
  })
  const parent2 = await models.User.create({
    username: 'leszekiksinski',
    email: 'leszekiksinski@sozi.com',
    password: 'qwertyuiop',
    firstName: 'Leszek',
    lastName: 'Iksiński',
    city: 'Warszawa',
    region: 'mazowieckie',
    isActive: true,
    phone: '321321321'
  })
  const child11 = await models.User.create({
    username: 'jerrydoe',
    email: 'jerrydoe@sozi.com',
    password: 'qwertyuiop',
    firstName: 'Jerry',
    lastName: 'Doe',
    city: 'Wrocław',
    region: 'dolnośląskie',
    isActive: true,
    birthday: DateTime.local().minus({years: 17, months: 11, days: 24}).toISODate(),
    phone: '999999999',
    inserterId: parent1.dataValues.id,
    updaterId: parent1.dataValues.id
  })
  const child12 = await models.User.create({
    username: 'johnnydoe',
    email: 'johnnydoe@sozi.com',
    password: 'qwertyuiop',
    firstName: 'Johnny',
    lastName: 'Doe',
    city: 'Wrocław',
    region: 'dolnośląskie',
    isActive: true,
    birthday: DateTime.local().minus({years: 17, months: 6, days: 12}).toISODate(),
    phone: '999999999',
    inserterId: parent1.dataValues.id,
    updaterId: parent1.dataValues.id
  })
  const child13 = await models.User.create({
    username: 'jareddoe',
    email: 'jareddoe@sozi.com',
    password: 'qwertyuiop',
    firstName: 'Jared',
    lastName: 'Doe',
    city: 'Wrocław',
    region: 'dolnośląskie',
    isActive: true,
    birthday: DateTime.local().minus({years: 17, months: 4, days: 16}).toISODate(),
    phone: '999999999',
    inserterId: parent1.dataValues.id,
    updaterId: parent1.dataValues.id
  })
  const child14 = await models.User.create({
    username: 'janetdoe',
    email: 'janetdoe@sozi.com',
    password: 'qwertyuiop',
    firstName: 'Janet',
    lastName: 'Doe',
    city: 'Wrocław',
    region: 'dolnośląskie',
    isActive: true,
    birthday: DateTime.local().minus({years: 17, months: 2, days: 13}).toISODate(),
    phone: '999999999',
    inserterId: parent1.dataValues.id,
    updaterId: parent1.dataValues.id
  })
  const child15 = await models.User.create({
    username: 'jeremiahdoe',
    email: 'jeremiahdoe@sozi.com',
    password: 'qwertyuiop',
    firstName: 'Jeremiah',
    lastName: 'Doe',
    city: 'Wrocław',
    region: 'dolnośląskie',
    isActive: true,
    birthday: DateTime.local().minus({years: 16, months: 11, days: 24}).toISODate(),
    phone: '999999999',
    inserterId: parent1.dataValues.id,
    updaterId: parent1.dataValues.id
  })
  const child16 = await models.User.create({
    username: 'jonathandoe',
    email: 'jonathandoe@sozi.com',
    password: 'qwertyuiop',
    firstName: 'Jonathan',
    lastName: 'Doe',
    city: 'Wrocław',
    region: 'dolnośląskie',
    isActive: true,
    birthday: DateTime.local().minus({years: 15, months: 11, days: 24}).toISODate(),
    phone: '999999999',
    inserterId: parent1.dataValues.id,
    updaterId: parent1.dataValues.id
  })
  const child21 = await models.User.create({
    username: 'jacekkowalski',
    email: 'jacekkowalski@sozi.com',
    password: 'qwertyuiop',
    firstName: 'Jacek',
    lastName: 'Kowalski',
    city: 'Warszawa',
    region: 'mazowieckie',
    isActive: true,
    birthday: DateTime.local().minus({years: 14, months: 11, days: 24}).toISODate(),
    phone: '999999999',
    inserterId: parent2.dataValues.id,
    updaterId: parent2.dataValues.id
  })
  const teacher = await models.User.create({
    username: 'jankowalski',
    email: 'jankowalski@sozi.com',
    password: 'qwertyuiop',
    firstName: 'Jan',
    lastName: 'Kowalski',
    city: 'Warszawa',
    region: 'mazowieckie',
    isActive: true,
    phone: '000000000'
  })
  const manager = await models.User.create({
    username: 'zbigniewnowak',
    email: 'zbigniewnowak@sozi.com',
    password: 'qwertyuiop',
    firstName: 'Zbigniew',
    lastName: 'Nowak',
    city: 'Warszawa',
    region: 'mazowieckie',
    isActive: true,
    phone: '123123123'
  })
  const unit1 = await models.EducationalUnit.create({
    name: 'Szkoła 1',
    email: 'sp1@sozi.com',
    city: 'Gliwice',
    region: 'śląskie',
    type: 'school',
    address: 'Kozielska 17',
    bankAccount: '00000000000000000000000000',
    description: 'Opis',
    website: 'https://www.sp1.pl',
    facebook: 'https://www.facebook.com/sp1'
  })
  const unit2 = await models.EducationalUnit.create({
    name: 'Szkoła 2',
    email: 'sp2@sozi.com',
    city: 'Warszawa',
    region: 'mazowieckie',
    type: 'school',
    address: 'Noakowskiego 2',
    bankAccount: '11111111111111111111111111',
    description: 'Opis',
    website: 'https://www.sp2.pl',
    facebook: 'https://www.facebook.com/sp2'
  })
  const unit3 = await models.EducationalUnit.create({
    name: 'Szkoła 3',
    email: 'sp3@sozi.com',
    city: 'Warszawa',
    region: 'mazowieckie',
    type: 'school',
    address: 'Noakowskiego 24',
    bankAccount: '33333333333333333333333333',
    description: 'Opis',
    website: 'https://www.sp3.pl',
    facebook: 'https://www.facebook.com/sp3'
  })
  const unit4 = await models.EducationalUnit.create({
    name: 'Szkoła 4',
    email: 'sp4@sozi.com',
    city: 'Warszawa',
    region: 'mazowieckie',
    type: 'school',
    address: 'Noakowskiego 67',
    bankAccount: '33333333333333333333333333',
    description: 'Opis',
    website: 'https://www.sp4.pl',
    facebook: 'https://www.facebook.com/sp4'
  })
  const unit5 = await models.EducationalUnit.create({
    name: 'Szkoła 5',
    email: 'sp5@sozi.com',
    city: 'Warszawa',
    region: 'mazowieckie',
    type: 'school',
    address: 'Noakowskiego 67',
    bankAccount: '33333333333333333333333333',
    description: 'Opis',
    website: 'https://www.sp5.pl',
    facebook: 'https://www.facebook.com/sp5'
  })
  const unit6 = await models.EducationalUnit.create({
    name: 'Szkoła 6',
    email: 'sp6@sozi.com',
    city: 'Gliwice',
    region: 'śląskie',
    type: 'school',
    address: 'Kozielska 27',
    bankAccount: '00000000000000000000000000',
    description: 'Opis',
    website: 'https://www.sp6.pl',
    facebook: 'https://www.facebook.com/sp6'
  })
  const unit7 = await models.EducationalUnit.create({
    name: 'Szkoła 7',
    email: 'sp7@sozi.com',
    city: 'Warszawa',
    region: 'mazowieckie',
    type: 'school',
    address: 'Noakowskiego 31',
    bankAccount: '11111111111111111111111111',
    description: 'Opis',
    website: 'https://www.sp7.pl',
    facebook: 'https://www.facebook.com/sp7'
  })
  const unit8 = await models.EducationalUnit.create({
    name: 'Szkoła 8',
    email: 'sp8@sozi.com',
    city: 'Warszawa',
    region: 'mazowieckie',
    type: 'school',
    address: 'Noakowskiego 86',
    bankAccount: '33333333333333333333333333',
    description: 'Opis',
    website: 'https://www.sp8.pl',
    facebook: 'https://www.facebook.com/sp8'
  })
  const unit9 = await models.EducationalUnit.create({
    name: 'Szkoła 9',
    email: 'sp9@sozi.com',
    city: 'Warszawa',
    region: 'mazowieckie',
    type: 'school',
    address: 'Noakowskiego 13',
    bankAccount: '33333333333333333333333333',
    description: 'Opis',
    website: 'https://www.sp9.pl',
    facebook: 'https://www.facebook.com/sp9'
  })
  const unit10 = await models.EducationalUnit.create({
    name: 'Szkoła 10',
    email: 'sp10@sozi.com',
    city: 'Warszawa',
    region: 'mazowieckie',
    type: 'school',
    address: 'Noakowskiego 1',
    bankAccount: '33333333333333333333333333',
    description: 'Opis',
    website: 'https://www.sp10.pl',
    facebook: 'https://www.facebook.com/sp10'
  })
  const unit11 = await models.EducationalUnit.create({
    name: 'Szkoła 11',
    email: 'sp11@sozi.com',
    city: 'Gliwice',
    region: 'śląskie',
    type: 'school',
    address: 'Kozielska 5',
    bankAccount: '00000000000000000000000000',
    description: 'Opis',
    website: 'https://www.sp11.pl',
    facebook: 'https://www.facebook.com/sp11'
  })
  const unit12 = await models.EducationalUnit.create({
    name: 'Szkoła 12',
    email: 'sp12@sozi.com',
    city: 'Warszawa',
    region: 'mazowieckie',
    type: 'school',
    address: 'Noakowskiego 99',
    bankAccount: '11111111111111111111111111',
    description: 'Opis',
    website: 'https://www.sp12.pl',
    facebook: 'https://www.facebook.com/sp12'
  })
  const unit13 = await models.EducationalUnit.create({
    name: 'Szkoła 13',
    email: 'sp13@sozi.com',
    city: 'Warszawa',
    region: 'mazowieckie',
    type: 'school',
    address: 'Noakowskiego 62',
    bankAccount: '33333333333333333333333333',
    description: 'Opis',
    website: 'https://www.sp13.pl',
    facebook: 'https://www.facebook.com/sp13'
  })
  const unit14 = await models.EducationalUnit.create({
    name: 'Szkoła 14',
    email: 'sp14@sozi.com',
    city: 'Warszawa',
    region: 'mazowieckie',
    type: 'school',
    address: 'Noakowskiego 23',
    bankAccount: '33333333333333333333333333',
    description: 'Opis',
    website: 'https://www.sp14.pl',
    facebook: 'https://www.facebook.com/sp14'
  })
  const unit15 = await models.EducationalUnit.create({
    name: 'Szkoła 15',
    email: 'sp15@sozi.com',
    city: 'Warszawa',
    region: 'mazowieckie',
    type: 'school',
    address: 'Noakowskiego 14',
    bankAccount: '33333333333333333333333333',
    description: 'Opis',
    website: 'https://www.sp15.pl',
    facebook: 'https://www.facebook.com/sp15'
  })
  const roleParent = await models.Role.findOne({where: {name: 'parent'}})
  const roleStudent = await models.Role.findOne({where: {name: 'student'}})
  const roleTeacher = await models.Role.findOne({where: {name: 'teacher'}})
  const roleManager = await models.Role.findOne({where: {name: 'manager'}})
  const subject1 = await models.Subject.create({name: 'historia', description: 'Nauka o dziejach państwa, narodu, społeczeństwa lub o procesie ich rozwoju.'})
  const subject2 = await models.Subject.create({name: 'matematyka', description: 'Nauka posługująca się metodą dedukcji, zajmująca się badaniem zbiorów liczb, punktów i innych elementów abstrakcyjnych.'})
  const subject3 = await models.Subject.create({name: 'fizyka', description: 'Nauka zajmująca się badaniem ogólnych właściwości materii i zjawisk w niej zachodzących.'})
  const subject4 = await models.Subject.create({name: 'informatyka', description: 'Nauka o tworzeniu i wykorzystywaniu systemów komputerowych.'})
  const subject5 = await models.Subject.create({name: 'biologia', description: 'Nauka o organizmach żywych.'})
  const course1 = await models.Course.create({
    defaultDuration: 45,
    defaultPrice: 0.00,
    defaultDay: ['Monday', 'Friday'],
    defaultTime: '18:45',
    title: 'Rzeczpospolita Obojga Narodów',
    description: 'Kurs omawia okres istnienia Rzeczpospolitej Obojga Narodów w latach 1569-1795.',
    isActive: true,
    subjectId: subject1.dataValues.id,
    teacherId: teacher.dataValues.id,
    educationalUnitId: unit2.dataValues.id
  })
  const course2 = await models.Course.create({
    defaultDuration: 45,
    defaultPrice: 0.00,
    defaultDay: ['Monday', 'Thursday'],
    defaultTime: '12:30',
    title: 'Tabliczka mnożenia',
    description: 'Kurs omawia sposoby obliczania tabliczki mnożenia.',
    isActive: true,
    subjectId: subject2.dataValues.id,
    teacherId: teacher.dataValues.id,
    educationalUnitId: unit2.dataValues.id
  })
  const course3 = await models.Course.create({
    defaultDuration: 60,
    defaultPrice: 10.00,
    defaultDay: ['Wednesday', 'Friday'],
    defaultTime: '10:00',
    title: 'Prawa Newtona',
    description: 'Kurs omawia zasady dynamiki zdefiniowane przez Newtona.',
    isActive: true,
    subjectId: subject3.dataValues.id,
    teacherId: teacher.dataValues.id,
    educationalUnitId: unit2.dataValues.id
  })
  const course4 = await models.Course.create({
    defaultDuration: 45,
    defaultPrice: 0.00,
    defaultDay: ['Monday'],
    defaultTime: '08:00',
    title: 'Podstawy C++',
    description: 'Kurs omawia podstawowy zakres programowania w języku C++.',
    isActive: true,
    subjectId: subject4.dataValues.id,
    teacherId: teacher.dataValues.id,
    educationalUnitId: unit2.dataValues.id
  })
  const course5 = await models.Course.create({
    defaultDuration: 45,
    defaultPrice: 0.00,
    defaultDay: ['Monday', 'Friday'],
    defaultTime: '16:15',
    title: 'Anatomia człowieka',
    description: 'Kurs omawia budowę i fizjologię człowieka.',
    isActive: true,
    subjectId: subject5.dataValues.id,
    teacherId: teacher.dataValues.id,
    educationalUnitId: unit2.dataValues.id
  })

  await parent1.setRoles([roleParent, roleTeacher])
  await parent2.setRoles([roleParent, roleManager])
  await child11.setRoles([roleStudent])
  await child12.setRoles([roleStudent])
  await child13.setRoles([roleStudent])
  await child14.setRoles([roleStudent])
  await child15.setRoles([roleStudent])
  await child16.setRoles([roleStudent])
  await child21.setRoles([roleStudent])
  await teacher.setRoles([roleTeacher])
  await manager.setRoles([roleManager])
  await parent1.setChildren([child11, child12, child13, child14, child15, child16])
  await parent1.addWorkUnit(unit2)
  await parent2.setChildren([child21])
  await parent2.setOwnedUnits([
    unit3,
    unit4,
    unit5,
    unit6,
    unit7,
    unit8,
    unit9,
    unit10,
    unit11,
    unit12,
    unit13,
    unit14,
    unit15
  ])
  await manager.addOwnedUnit(unit2)
  await admin.addOwnedUnit(unit1)
  await teacher.addWorkUnit(unit2)
  await models.TeacherEducationalUnit.update({status: 'active'}, {where: {teacherId: teacher.dataValues.id, educationalUnitId: unit2.dataValues.id}})
  await unit2.setStudents([child11, child12, child13, child14, child15, child16])
  await models.StudentEducationalUnit.update({status: 'active'}, {where: {educationalUnitId: unit2.dataValues.id}})
  await unit3.setStudents([child11, child12, child13, child14, child15, child16])
  await course1.setStudents([child11, child12, child13])
  await course2.setStudents([child14, child15, child16])
  await course3.setStudents([child12, child14, child16])
  await course4.setStudents([child11, child13, child15])
  await course5.setStudents([child11, child12, child13])

  await models.Lesson.create({
    date: DateTime.local().set({hours: +course1.dataValues.defaultTime.substring(0, 2), minutes: +course1.dataValues.defaultTime.substring(3), seconds: 0}).plus({days: 1}),
    duration: course1.dataValues.defaultDuration,
    price: course1.dataValues.defaultPrice,
    paymentDeadline: DateTime.local().set({hours: 0, minutes: 0, seconds: 0}).plus({days: 7}),
    paymentStatus: course1.dataValues.defaultPrice === 0,
    homework: null,
    teacherNotes: null,
    messageForParent: null,
    studentId: child11.dataValues.id,
    courseId: course1.dataValues.id
  })
  await models.Lesson.create({
    date: DateTime.local().set({hours: +course1.dataValues.defaultTime.substring(0, 2), minutes: +course1.dataValues.defaultTime.substring(3), seconds: 0}).plus({days: 3}),
    duration: course1.dataValues.defaultDuration,
    price: course1.dataValues.defaultPrice,
    paymentDeadline: DateTime.local().set({hours: 0, minutes: 0, seconds: 0}).plus({days: 11}),
    paymentStatus: course1.dataValues.defaultPrice === 0,
    homework: null,
    teacherNotes: null,
    messageForParent: null,
    studentId: child12.dataValues.id,
    courseId: course1.dataValues.id
  })
  await models.Lesson.create({
    date: DateTime.local().set({hours: +course1.dataValues.defaultTime.substring(0, 2), minutes: +course1.dataValues.defaultTime.substring(3), seconds: 0}).plus({days: 4}),
    duration: course1.dataValues.defaultDuration,
    price: course1.dataValues.defaultPrice,
    paymentDeadline: DateTime.local().set({hours: 0, minutes: 0, seconds: 0}).plus({days: 12}),
    paymentStatus: course1.dataValues.defaultPrice === 0,
    homework: null,
    teacherNotes: null,
    messageForParent: null,
    studentId: child13.dataValues.id,
    courseId: course1.dataValues.id
  })
  await models.Lesson.create({
    date: DateTime.local().set({hours: +course2.dataValues.defaultTime.substring(0, 2), minutes: +course2.dataValues.defaultTime.substring(3), seconds: 0}).plus({days: 2}),
    duration: course2.dataValues.defaultDuration,
    price: course2.dataValues.defaultPrice,
    paymentDeadline: DateTime.local().set({hours: 0, minutes: 0, seconds: 0}).plus({days: 9}),
    paymentStatus: course2.dataValues.defaultPrice === 0,
    homework: null,
    teacherNotes: null,
    messageForParent: null,
    studentId: child14.dataValues.id,
    courseId: course2.dataValues.id
  })
}
