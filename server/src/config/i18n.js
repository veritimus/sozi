const path = require('path')
const i18next = require('i18next')
const FsBackend = require('i18next-node-fs-backend')

function middlewareSetLang (req, res, next) {
  req.i18n.changeLanguage(req.headers.language)
  next()
}

i18next.use(FsBackend).init({
  fallbackLng: 'pl',
  load: 'languageOnly',
  preload: ['en', 'pl', 'de'],
  backend: {
    loadPath: path.join(__dirname, '../locales/{{lng}}/{{ns}}.json')
  },
  useCookie: false,
  debug: false
})

module.exports = {i18next, middlewareSetLang}
