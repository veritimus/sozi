const CronJob = require('cron').CronJob
const db = require('../models')
const {DateTime} = require('luxon')

const childVerification = new CronJob('00 00 02 * * 0-6', async function () {
  const {Op, col} = db.sequelize
  const children = await db.User.findAll({
    attributes: ['id', 'firstName', 'lastName', 'email', 'birthday'],
    include: {
      model: db.Role,
      as: 'Roles',
      attributes: ['id'],
      through: {
        attributes: ['id'],
        where: {
          isValid: true
        }
      }
    },
    where: {
      [Op.and]: [
        {'$Roles.id$': 2},
        {'$Roles.id$': {[Op.ne]: 3}},
        {birthday: {[Op.not]: null}}
      ]
    },
    order: [[col('birthday'), 'ASC']],
    limit: 100,
    subQuery: false
  })
  if (children.length > 0) {
    const adultChildren = children.filter(child => parseInt(DateTime.local().diff(DateTime.fromISO(child.dataValues.birthday), 'years').toObject().years) >= 18)
    const roleParent = await db.Role.findById(3)
    await Promise.all(adultChildren.map(child => child.addRole(roleParent)))
  }
}, null, false, 'Europe/Warsaw')

module.exports = {
  childVerification
}
