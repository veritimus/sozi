const passportJWT = require('passport-jwt')
const ExtractJWT = passportJWT.ExtractJwt
const LocalStrategy = require('passport-local').Strategy
const JWTStrategy = passportJWT.Strategy
const db = require('../models/')
const {authKeys} = require('./')

module.exports = (passport) => {
  passport.use(new LocalStrategy(
    async (username, password, done) => {
      try {
        const user = await db.User.authentication({username, password})
        if (user) {
          return done(null, user)
        } else {
          return done(null, user, {message: 'Invalid username or password'})
        }
      } catch (err) {
        done(err)
      }
    }
  ))

  passport.use(new JWTStrategy({
    jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    secretOrKey: authKeys.secret
  }, async (jwtPayload, done) => {
    try {
      const user = await db.User.findById(jwtPayload.id)
      return done(null, user.dataValues)
    } catch (err) {
      return done(err)
    }
  }))
}
