const express = require('express')
const bodyParser = require('body-parser')
const fileUpload = require('express-fileupload')
const cors = require('cors')
const serveStatic = require('serve-static')
const history = require('connect-history-api-fallback')
const morgan = require('morgan')
const passport = require('passport')
const {sequelize} = require('./models')
const {childVerification} = require('./config/cron')
const routes = require('./routes')
const {i18next, middlewareSetLang} = require('./config/i18n')
const i18nextMiddleware = require('i18next-express-middleware')
const insertDevData = require('./config/devData')
const {adminAccount, port} = require('./config')
require('./config/passport')(passport)

const app = express()
app.use(i18nextMiddleware.handle(i18next))
app.use(middlewareSetLang)
app.use(morgan('tiny'))
app.use(bodyParser.json({limit: '2mb'}))
app.use(cors())
app.use(fileUpload())
app.use(history())
app.use(serveStatic('/app/client/dist'))

app.use('/auth', routes.auth)
app.use('/api/v1/users', passport.authenticate('jwt', {session: false}), routes.users)
app.use('/api/v1/children', passport.authenticate('jwt', {session: false}), routes.children)
app.use('/api/v1/courses', passport.authenticate('jwt', {session: false}), routes.courses)
app.use('/api/v1/eduUnits', passport.authenticate('jwt', {session: false}), routes.eduUnits)
app.use('/api/v1/managers', passport.authenticate('jwt', {session: false}), routes.managers)
app.use('/api/v1/lessons', passport.authenticate('jwt', {session: false}), routes.lessons)
app.use('/api/v1/students', passport.authenticate('jwt', {session: false}), routes.students)
app.use('/api/v1/subjects', passport.authenticate('jwt', {session: false}), routes.subjects)
app.use('/api/v1/teachers', passport.authenticate('jwt', {session: false}), routes.teachers)

if (process.env.NODE_ENV === 'production') {
  app.get('*', (req, res) => {
    res.sendFile('/app/client/dist/index.html')
  })
}

sequelize.sync()
  .then(async () => {
    const hasRoles = await sequelize.models.Role.findOne({where: {id: 1}})
    if (!hasRoles) {
      await sequelize.models.Role.bulkCreate([
        {name: 'admin', isActive: true},
        {name: 'student', isActive: true},
        {name: 'parent', isActive: true},
        {name: 'teacher', isActive: true},
        {name: 'manager', isActive: true}
      ])
    }
    const hasAdmin = await sequelize.models.UserRole.findOne({where: {roleId: 1}})
    if (!hasAdmin) {
      const admin = await sequelize.models.User.create({
        username: adminAccount.username,
        email: adminAccount.email,
        password: adminAccount.password,
        firstName: adminAccount.firstName,
        lastName: adminAccount.lastName,
        city: adminAccount.city,
        region: adminAccount.region,
        isActive: true,
        phone: adminAccount.phone
      })
      await sequelize.models.UserRole.bulkCreate([
        {userId: admin.dataValues.id, roleId: 1},
        {userId: admin.dataValues.id, roleId: 3},
        {userId: admin.dataValues.id, roleId: 4},
        {userId: admin.dataValues.id, roleId: 5}
      ])
      if (process.env.NODE_ENV !== 'production') {
        await insertDevData(sequelize.models, admin)
      }
    }

    childVerification.start()
    app.listen(port)
    console.log(`Server started on port ${port}`)
  })
  .catch(err => {
    console.log(err)
  })
