const express = require('express')
const router = express.Router()
const ManagersController = require('../controllers/ManagersController')

router.get(
  '/:id/eduUnits',
  ManagersController.fetchUnits
)

module.exports = router
