const express = require('express')
const router = express.Router()
const TeachersController = require('../controllers/TeachersController')

router.get(
  '/:id/lessons',
  TeachersController.fetchLessons
)

router.get(
  '/:id/eduUnits',
  TeachersController.fetchUnits
)

module.exports = router
