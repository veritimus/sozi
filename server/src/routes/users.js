const express = require('express')
const router = express.Router()
const UsersController = require('../controllers/UsersController')

router.get(
  '/:id',
  UsersController.fetchUser
)

router.put(
  '/:id',
  UsersController.updateUser
)

router.post(
  '/:id/children',
  UsersController.createChildAccount
)

router.get(
  '/:id/children',
  UsersController.fetchChildren
)

router.get(
  '/:id/childrenLessons',
  UsersController.fetchChildrenLessons
)

router.get(
  '/:id/lessons',
  UsersController.fetchUserLessons
)

router.get(
  '/:id/eduUnits',
  UsersController.fetchUserUnits
)

module.exports = router
