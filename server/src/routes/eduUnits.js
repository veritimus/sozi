const express = require('express')
const router = express.Router()
const EduUnitsController = require('../controllers/EduUnitsController')

router.get(
  '/',
  EduUnitsController.fetchUnits
)

router.post(
  '/',
  EduUnitsController.createUnit
)

router.get(
  '/:id',
  EduUnitsController.fetchUnit
)

router.put(
  '/:id',
  EduUnitsController.updateUnit
)

router.get(
  '/:id/courses',
  EduUnitsController.fetchCourses
)

router.post(
  '/:id/courses',
  EduUnitsController.createCourse
)

router.put(
  '/:id/courses/:courseId',
  EduUnitsController.changeCourseStatus
)

router.post(
  '/:id/courses/:courseId/lessons',
  EduUnitsController.addStudentToCourse
)

router.get(
  '/:id/lessons',
  EduUnitsController.fetchLessons
)

router.get(
  '/:id/students',
  EduUnitsController.fetchStudents
)

router.post(
  '/:id/students',
  EduUnitsController.addStudent
)

router.put(
  '/:id/students/:userId',
  EduUnitsController.changeStudentStatus
)

router.get(
  '/:id/teachers',
  EduUnitsController.fetchTeachers
)

router.post(
  '/:id/teachers',
  EduUnitsController.addTeacher
)

router.put(
  '/:id/teachers/:userId',
  EduUnitsController.changeTeacherStatus
)

router.get(
  '/:id/teachers/:userId/courses',
  EduUnitsController.fetchTeacherCourses
)

module.exports = router
