const express = require('express')
const router = express.Router()
const ChildrenController = require('../controllers/ChildrenController')

router.get(
  '/',
  ChildrenController.fetchStudents
)

module.exports = router
