const express = require('express')
const router = express.Router()
const StudentsController = require('../controllers/StudentsController')

router.get(
  '/',
  StudentsController.fetchStudents
)

module.exports = router
