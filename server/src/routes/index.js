module.exports = {
  auth: require('./auth'),
  users: require('./users'),
  children: require('./children'),
  courses: require('./courses'),
  eduUnits: require('./eduUnits'),
  managers: require('./managers'),
  lessons: require('./lessons'),
  students: require('./students'),
  subjects: require('./subjects'),
  teachers: require('./teachers')
}
