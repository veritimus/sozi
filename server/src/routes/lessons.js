const express = require('express')
const router = express.Router()
const LessonsController = require('../controllers/LessonsController')

router.get(
  '/:id',
  LessonsController.fetchLesson
)

router.put(
  '/:id',
  LessonsController.updateLesson
)

module.exports = router
