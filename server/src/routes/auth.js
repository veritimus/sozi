const express = require('express')
const router = express.Router()
const AuthController = require('../controllers/AuthController')

router.get(
  '/',
  AuthController.checkIfExist
)

router.post(
  '/register',
  AuthController.register
)

router.post(
  '/login',
  AuthController.login
)

router.get(
  '/user/:token',
  AuthController.verifyUser
)

router.post(
  '/user/generate_password',
  AuthController.sendNewPassword
)

router.post(
  '/user/generate_token',
  AuthController.sendNewValidityToken
)

module.exports = router
