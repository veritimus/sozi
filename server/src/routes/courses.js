const express = require('express')
const router = express.Router()
const CoursesController = require('../controllers/CoursesController')

router.get(
  '/',
  CoursesController.fetchCourses
)

router.get(
  '/:id',
  CoursesController.fetchCourse
)

router.put(
  '/:id',
  CoursesController.updateCourse
)

router.post(
  '/:id/lessons',
  CoursesController.createLesson
)

router.get(
  '/:id/students',
  CoursesController.fetchStudents
)

module.exports = router
