const jwt = require('jsonwebtoken')
const {userValidationKeys} = require('../../config')
const db = require('../../models')
const {DateTime} = require('luxon')
const generator = require('generate-password')

const courseIncludes = function () {
  return [
    {
      model: db.Subject,
      attributes: ['name']
    }, {
      model: db.EducationalUnit,
      attributes: ['id', 'name'],
      include: [{
        model: db.User,
        as: 'managers',
        attributes: ['id']
      }]
    }, {
      model: db.User,
      as: 'Teacher',
      attributes: ['id', 'firstName', 'lastName']
    }
  ]
}

const eduUnitIncludes = function (id) {
  return [
    {
      model: db.User,
      as: 'managers',
      attributes: ['id', 'firstName', 'lastName'],
      through: {
        where: {
          educationalUnitId: id
        }
      }
    }, {
      model: db.User,
      as: 'teachers',
      attributes: ['id'],
      through: {
        where: {
          educationalUnitId: id
        }
      }
    }, {
      model: db.User,
      as: 'students',
      attributes: ['id'],
      through: {
        where: {
          educationalUnitId: id
        }
      }
    }, {
      model: db.Course,
      attributes: ['id']
    }
  ]
}

const lessonIncludes = function () {
  return [
    {
      model: db.User,
      as: 'student',
      attributes: ['id', 'firstName', 'lastName', 'city', 'parentId']
    }, {
      model: db.User,
      as: 'updater',
      attributes: ['id', 'firstName', 'lastName']
    }, {
      model: db.Course,
      attributes: ['id', 'title'],
      include: courseIncludes()
    }
  ]
}

const lessonScheduleIncludes = function () {
  return [
    {
      model: db.User,
      as: 'student',
      attributes: ['id', 'parentId', 'firstName', 'lastName', 'city'],
      include: [{
        model: db.Role,
        attributes: ['name'],
        through: {
          attributes: ['isValid', 'roleId']
        }
      }]
    }, {
      model: db.Course,
      attributes: ['title'],
      include: [
        {
          model: db.Subject,
          attributes: ['name']
        }, {
          model: db.EducationalUnit,
          attributes: ['name']
        }, {
          model: db.User,
          as: 'Teacher',
          attributes: ['firstName', 'lastName']
        }
      ]
    }
  ]
}

const userAttributesSetup = function () {
  return {exclude: ['password', 'validityToken']}
}

const userIncludes = function () {
  return [
    {
      model: db.User,
      as: 'children',
      attributes: userAttributesSetup(),
      include: [
        {
          model: db.Role,
          attributes: ['name'],
          through: {
            attributes: ['isValid', 'roleId']
          }
        }, {
          model: db.Lesson,
          include: [{
            model: db.Course,
            attributes: ['id'],
            include: [{
              model: db.Subject,
              attributes: ['name']
            }]
          }],
          sort: [db.sequelize.col('date', 'DESC')],
          limit: 1
        }
      ]
    }, {
      model: db.Role,
      attributes: ['name'],
      through: {
        attributes: ['isValid']
      }
    }, {
      model: db.Lesson,
      include: [{
        model: db.Course,
        attributes: ['id'],
        include: [{
          model: db.Subject,
          attributes: ['name']
        }]
      }],
      sort: [db.sequelize.col('date', 'DESC')],
      limit: 1
    }, {
      model: db.Course,
      attributes: ['id'],
      include: [
        {
          model: db.Subject,
          attributes: ['name']
        }, {
          model: db.Lesson,
          include: [{
            model: db.User,
            as: 'student',
            attributes: ['id', 'firstName', 'lastName']
          }],
          sort: [db.sequelize.col('date', 'DESC')],
          limit: 1
        }
      ]
    }
  ]
}

module.exports = {
  courseIncludes,

  async createSubject (formData) {
    if (!formData.id) {
      const subject = await db.Subject.create({
        name: formData.name || null,
        description: formData.description || null
      })
      return subject.dataValues.id
    }
    return null
  },

  async createUserRoles (roles, id) {
    await roles.forEach(async role => {
      const roleId = await db.Role.findOne({where: {name: role}})
      await db.UserRole.create({userId: id, roleId: roleId.dataValues.id})
    })
  },

  eduUnitIncludes,

  filterNotAdultChildren (children) {
    return children.filter(child => !child.dataValues.Roles
      .some(role => role.dataValues.UserRole.dataValues.roleId === 3 && role.dataValues.UserRole.dataValues.isValid === true))
  },

  generatePassword () {
    return generator.generate({
      length: 8,
      numbers: true,
      strict: true
    })
  },

  generateValidityToken (username) {
    return jwt.sign({
      username: username
    }, userValidationKeys.secret, {
      expiresIn: userValidationKeys.tokenValidityTime
    })
  },

  getErrorMessages (error, messages = {}) {
    if (error.errors !== undefined) {
      error.errors.forEach(error => {
        messages[error.path] = error.message
      })
    } else {
      messages['error'] = error.message
    }
    return messages
  },

  getLessonSchedule ({lessonQuantity, days, time, fromDate}) {
    const hour = parseInt(time.substring(0, 2))
    const minute = parseInt(time.substring(2))
    const fromDay = DateTime.fromISO(fromDate, {zone: 'Europe/Warsaw'})
    const schedule = []
    let counter = 0
    while (lessonQuantity > schedule.length) {
      const day = fromDay.set({hour, minute, second: 0, millisecond: 0}).plus({days: counter})
      if (days.includes(day.toFormat('EEEE'))) {
        schedule.push(day.toISO())
      }
      counter++
    }
    return schedule
  },

  getUpdateData (data) {
    const updateData = {}
    Object.keys(data).forEach(key => {
      if (!data[key]) {
        updateData[key] = null
      } else {
        updateData[key] = data[key]
      }
    })
    return updateData
  },

  lessonIncludes,

  lessonScheduleIncludes,

  userAttributesSetup,

  userIncludes
}
