const db = require('../models')
const {courseIncludes, getErrorMessages, getUpdateData, userAttributesSetup} = require('./helpers')

module.exports = {
  async createLesson (req, res) {
    try {
      const newLesson = await db.Lesson.create({
        date: req.body.date || null,
        duration: req.body.duration || null,
        price: req.body.price || null,
        paymentDeadline: req.body.paymentDeadline || null,
        paymentStatus: parseFloat(req.body.price) === 0,
        homework: req.body.homework || null,
        teacherNotes: req.body.teacherNotes || null,
        messageForParent: req.body.messageForParent || null,
        studentId: req.body.studentId || null,
        courseId: req.params.id || null
      })
      const lesson = await db.Lesson.findById(newLesson.dataValues.id, {
        include: [
          {
            model: db.User,
            as: 'student',
            attributes: ['id', 'firstName', 'lastName', 'city']
          }, {
            model: db.Course,
            attributes: ['title'],
            include: [
              {
                model: db.Subject,
                attributes: ['name']
              }, {
                model: db.EducationalUnit,
                attributes: ['name']
              }, {
                model: db.User,
                as: 'Teacher',
                attributes: ['firstName', 'lastName']
              }
            ]
          }
        ]
      })
      res.send({
        lesson,
        message: req.i18n.t('messages.success.lessonRegistrationCompleted'),
        type: 'success'
      })
    } catch (error) {
      if (error.name === 'UserDateNotAvailable') {
        error.message = req.i18n.t('messages.error.userDateNotAvailable')
      }
      res.status(400).send({error: getErrorMessages(error)})
    }
  },

  async fetchCourse (req, res) {
    try {
      const course = await db.Course.findById(req.params.id, {include: courseIncludes()})
      res.send({course})
    } catch (error) {
      res.status(400).send({error})
    }
  },

  async fetchCourses (req, res) {
    try {
      const title = req.query.title || ''
      const subjectName = req.query.subjectName || ''
      const minPrice = +req.query.minPrice || 0
      const maxPrice = +req.query.maxPrice || 9999.00
      const unitName = req.query.unitName || ''
      const city = req.query.city || ''
      const region = req.query.region || ''
      const {Op} = db.sequelize
      const conditionals = {
        [Op.and]: [
          {title: {[Op.iLike]: `%${title}%`}},
          {defaultPrice: {[Op.between]: [+minPrice, +maxPrice]}},
          {'$Subject.name$': {[Op.iLike]: `%${subjectName}%`}},
          {'$EducationalUnit.name$': {[Op.iLike]: `%${unitName}%`}},
          {'$EducationalUnit.city$': {[Op.iLike]: `${city}%`}},
          {'$EducationalUnit.region$': {[Op.iLike]: `${region}%`}},
          {'$EducationalUnit.type$': {[Op.in]: req.query.types.split(',')}}
        ]
      }
      const courses = await db.Course.findAndCountAll({
        include: [
          {
            model: db.EducationalUnit,
            attributes: ['id', 'name', 'city', 'region', 'type']
          }, {
            model: db.Subject,
            attributes: ['name']
          }, {
            model: db.User,
            as: 'Teacher',
            attributes: userAttributesSetup()
          }
        ],
        where: conditionals,
        limit: req.query.rows,
        offset: req.query.page * req.query.rows
      })
      res.send({courses: courses.rows, totalCourses: courses.count})
    } catch (error) {
      res.status(400).send({error})
    }
  },

  async fetchStudents (req, res) {
    try {
      const students = await db.User.findAll({})
      res.send({students})
    } catch (error) {
      res.status(400).send({error})
    }
  },

  async updateCourse (req, res) {
    try {
      const updateData = getUpdateData(req.body)
      await db.Course.update(updateData, {where: {id: req.params.id}})
      const course = await db.Course.findById(req.params.id, {include: courseIncludes()})
      res.send({
        course,
        message: req.i18n.t('messages.success.courseUpdatingCompleted'),
        type: 'success'
      })
    } catch (error) {
      res.status(400).send({error: getErrorMessages(error)})
    }
  }
}
