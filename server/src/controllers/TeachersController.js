const db = require('../models')

module.exports = {
  async fetchLessons (req, res) {
    try {
      const {startDate, endDate, status} = req.query
      const {Op} = db.sequelize
      if (status === 'all') {
        const lessons = await db.Lesson.findAll({
          include: [
            {
              model: db.User,
              as: 'student',
              attributes: ['id', 'firstName', 'lastName', 'city']
            }, {
              model: db.Course,
              attributes: ['title'],
              include: [
                {
                  model: db.Subject,
                  attributes: ['name']
                }, {
                  model: db.EducationalUnit,
                  attributes: ['name']
                }
              ],
              where: {
                teacherId: req.params.id
              }
            }
          ],
          where: {
            date: {
              [Op.between]: [startDate, endDate]
            }
          }
        })
        return res.send({lessons})
      }
      const lessons = await db.Lesson.findAll({
        include: [
          {
            model: db.User,
            as: 'student',
            attributes: ['id', 'firstName', 'lastName', 'city']
          }, {
            model: db.Course,
            attributes: ['title'],
            include: [
              {
                model: db.Subject,
                attributes: ['name']
              }, {
                model: db.EducationalUnit,
                attributes: ['name']
              }
            ],
            where: {
              teacherId: req.params.id
            }
          }
        ],
        where: {
          isActive: status === 'active',
          date: {
            [Op.between]: [startDate, endDate]
          }
        }
      })
      res.send({lessons})
    } catch (error) {
      res.status(400).send({error})
    }
  },

  async fetchUnits (req, res) {
    try {
      const units = await db.EducationalUnit.findAll({
        attributes: {exclude: ['logo']},
        include: [{
          model: db.User,
          as: 'teachers',
          attributes: [],
          through: {
            attributes: ['status']
          }
        }],
        where: {
          '$teachers.id$': req.params.id,
          '$teachers.TeacherEducationalUnit.status$': 'active'
        }
      })
      res.send({units, totalUnits: units.length})
    } catch (error) {
      res.status(400).send({error})
    }
  }
}
