const db = require('../models')
const {DateTime} = require('luxon')
const {createSubject, eduUnitIncludes, getErrorMessages, getLessonSchedule, getUpdateData, lessonIncludes, userAttributesSetup} = require('./helpers')

module.exports = {
  async addStudent (req, res) {
    try {
      const unitStudent = await db.StudentEducationalUnit.find({
        where: {
          studentId: req.body.userId,
          educationalUnitId: req.params.id
        }
      })
      if (unitStudent) {
        if (unitStudent.dataValues.status === 'active' || unitStudent.dataValues.status === 'pending') {
          if (req.body.userType === 'child') {
            return res.send({
              message: req.i18n.t('messages.info.eduUnitChildAddingAlreadyAdded'),
              type: 'warning'
            })
          } else {
            return res.send({
              message: req.i18n.t('messages.info.eduUnitStudentAddingAlreadyAdded'),
              type: 'warning'
            })
          }
        } else if (unitStudent.dataValues.status === 'inactive') {
          await unitStudent.update({status: 'pending'})
        }
      } else {
        await db.StudentEducationalUnit.create({studentId: req.body.userId, educationalUnitId: req.params.id})
      }
      if (req.body.userType === 'child') {
        return res.send({
          message: req.i18n.t('messages.success.eduUnitChildAddingCompleted'),
          type: 'success'
        })
      }
      res.send({
        message: req.i18n.t('messages.success.eduUnitStudentAddingCompleted'),
        type: 'success'
      })
    } catch (error) {
      res.status(400).send({error})
    }
  },

  async addStudentToCourse (req, res) {
    try {
      const {teacherId, studentId, duration, price, ...scheduleData} = req.body
      const schedule = getLessonSchedule(scheduleData)
      const lessons = []
      const course = await db.Course.findById(req.params.courseId)
      const student = await db.User.findById(studentId)
      await Promise.all(schedule.map(async date => {
        const lesson = await db.Lesson.create({
          date: date || null,
          duration: duration || null,
          price: price || null,
          paymentStatus: parseFloat(price) === 0,
          paymentDeadline: DateTime.fromISO(date).plus({days: 7}).toISO(),
          courseId: req.params.courseId,
          studentId: studentId || null
        })
        lessons.push(lesson)
      }))
      await course.addStudent(student)
      if (course.dataValues.teacherId !== teacherId) {
        await course.setTeacher(await db.User.findById(teacherId))
      }
      res.send({
        lessons,
        message: req.i18n.t('messages.success.courseStudentAddingCompleted'),
        type: 'success'
      })
    } catch (error) {
      if (error.name === 'UserDateNotAvailable') {
        error.message = req.i18n.t('messages.error.userDatesNotAvailable')
      }
      res.status(400).send({error: getErrorMessages(error)})
    }
  },

  async addTeacher (req, res) {
    try {
      const unitTeacher = await db.TeacherEducationalUnit.find({
        where: {
          teacherId: req.body.userId,
          educationalUnitId: req.params.id
        }
      })
      if (unitTeacher) {
        if (unitTeacher.dataValues.status === 'active' || unitTeacher.dataValues.status === 'pending') {
          return res.send({
            message: req.i18n.t('messages.info.eduUnitTeacherAddingAlreadyAdded'),
            type: 'warning'
          })
        } else if (unitTeacher.dataValues.status === 'inactive') {
          await unitTeacher.update({status: 'pending'})
        }
      } else {
        await db.TeacherEducationalUnit.create({teacherId: req.body.userId, educationalUnitId: req.params.id})
      }
      res.send({
        message: req.i18n.t('messages.success.eduUnitTeacherAddingCompleted'),
        type: 'success'
      })
    } catch (error) {
      res.status(400).send({error})
    }
  },

  async changeCourseStatus (req, res) {
    try {
      const unitCourse = await db.Course.update({isActive: req.body.status}, {
        where: {
          id: req.params.courseId,
          educationalUnitId: req.params.id
        },
        returning: true
      })
      res.send({
        course: unitCourse[1][0],
        message: req.i18n.t('messages.success.eduUnitCourseStatusChangingCompleted'),
        type: 'success'
      })
    } catch (error) {
      res.status(400).send({error: req.i18n.t('messages.error.eduUnitCourseStatusChangingFailed')})
    }
  },

  async changeStudentStatus (req, res) {
    try {
      const unitStudent = await db.StudentEducationalUnit.update({
        status: req.body.status,
        updaterId: req.body.updater
      }, {
        where: {
          studentId: req.params.userId,
          educationalUnitId: req.params.id
        }
      })
      res.send({
        unitStudent,
        message: req.i18n.t('messages.success.eduUnitStudentStatusChangingCompleted'),
        type: 'success'
      })
    } catch (error) {
      res.status(400).send({error: req.i18n.t('messages.error.eduUnitStudentStatusChangingFailed')})
    }
  },

  async changeTeacherStatus (req, res) {
    try {
      const unitTeacher = await db.TeacherEducationalUnit.update({
        status: req.body.status,
        updaterId: req.body.updater
      }, {
        where: {
          teacherId: req.params.userId,
          educationalUnitId: req.params.id
        }
      })
      res.send({
        unitTeacher,
        message: req.i18n.t('messages.success.eduUnitTeacherStatusChangingCompleted'),
        type: 'success'
      })
    } catch (error) {
      res.status(400).send({error: req.i18n.t('messages.error.eduUnitTeacherStatusChangingFailed')})
    }
  },

  async createCourse (req, res) {
    try {
      const subjectId = await createSubject(req.body.subject)
      let defaultDay
      if (req.body.defaultDay.length > 0) {
        defaultDay = req.body.defaultDay
      } else {
        defaultDay = null
      }
      const newCourse = await db.Course.create({
        defaultDuration: req.body.defaultDuration || null,
        defaultPrice: parseFloat(req.body.defaultPrice),
        defaultDay: defaultDay,
        defaultTime: req.body.defaultTime || null,
        lessonsQuantity: req.body.lessonsQuantity || null,
        title: req.body.title || null,
        description: req.body.description || null,
        teacherId: req.body.teacherId || null,
        educationalUnitId: req.params.id || null,
        subjectId: req.body.subject.id || subjectId || null
      })
      const course = await db.Course.findById(newCourse.dataValues.id, {
        include: [
          {
            model: db.Subject
          }, {
            model: db.EducationalUnit,
            attributes: ['id', 'name']
          }
        ]
      })
      res.send({
        course,
        message: req.i18n.t('messages.success.courseRegistrationCompleted'),
        type: 'success'
      })
    } catch (error) {
      res.status(400).send({error: getErrorMessages(error)})
    }
  },

  async createUnit (req, res) {
    try {
      const user = await db.User.findById(req.body.userId)
      const unit = await db.EducationalUnit.create({
        name: req.body.name || null,
        email: req.body.email || null,
        city: req.body.city || null,
        region: req.body.region || null,
        type: req.body.type || null,
        bankAccount: req.body.bankAccount || null,
        logo: req.body.file ? Buffer.from(req.body.file, 'base64') : null,
        description: req.body.description || null,
        address: req.body.address || null,
        website: req.body.website || null,
        facebook: req.body.facebook || null,
        phone: req.body.phone || null
      })
      await user.addOwnedUnit(unit)
      res.send({
        unit,
        message: req.i18n.t('messages.success.eduUnitRegistrationCompleted'),
        type: 'success'
      })
    } catch (error) {
      res.status(400).send({error: getErrorMessages(error)})
    }
  },

  async fetchCourses (req, res) {
    try {
      const {status} = req.query
      let courses
      if (status === 'manager') {
        courses = await db.Course.findAll({
          include: [
            {
              model: db.Subject,
              as: 'Subject',
              attributes: ['name']
            }, {
              model: db.User,
              as: 'students',
              attributes: userAttributesSetup()
            }
          ],
          where: {
            isActive: true,
            educationalUnitId: req.params.id
          }
        })
        return res.send({courses})
      } else if (status === 'all') {
        courses = await db.Course.findAndCountAll({
          include: [
            {
              model: db.EducationalUnit,
              attributes: ['id', 'name']
            }, {
              model: db.Subject,
              attributes: ['name', 'description']
            }
          ],
          where: {
            '$EducationalUnit.id$': req.params.id
          }
        })
      } else {
        courses = await db.Course.findAndCountAll({
          include: [
            {
              model: db.EducationalUnit,
              attributes: ['id', 'name']
            }, {
              model: db.Subject,
              attributes: ['name', 'description']
            }
          ],
          where: {
            '$EducationalUnit.id$': req.params.id,
            isActive: status === 'active'
          }
        })
      }
      res.send({courses: courses.rows, totalCourses: courses.count})
    } catch (error) {
      res.status(400).send({error})
    }
  },

  async fetchLessons (req, res) {
    try {
      const {startDate, endDate, status} = req.query
      const {Op} = db.sequelize
      if (status === 'all') {
        const lessons = await db.Lesson.findAll({
          include: lessonIncludes(),
          where: {
            '$Course.EducationalUnit.id$': req.params.id,
            date: {
              [Op.between]: [startDate, endDate]
            }
          }
        })
        return res.send({lessons})
      }
      const lessons = await db.Lesson.findAll({
        include: lessonIncludes(),
        where: {
          '$Course.EducationalUnit.id$': req.params.id,
          date: {
            [Op.between]: [startDate, endDate]
          },
          isActive: status === 'active'
        }
      })
      res.send({lessons})
    } catch (error) {
      res.status(400).send({error})
    }
  },

  async fetchStudents (req, res) {
    try {
      if (req.query) {
        let students
        if (req.query.status) {
          const {status} = req.query
          if (status === 'all') {
            students = await db.User.findAndCountAll({
              attributes: userAttributesSetup(),
              include: [{
                model: db.EducationalUnit,
                as: 'schools',
                attributes: ['id'],
                through: {
                  as: 'school',
                  attributes: ['status']
                }
              }],
              where: {
                '$schools.id$': req.params.id
              }
            })
          } else {
            students = await db.User.findAndCountAll({
              attributes: userAttributesSetup(),
              include: [{
                model: db.EducationalUnit,
                as: 'schools',
                attributes: ['id'],
                through: {
                  as: 'school',
                  attributes: ['status']
                }
              }],
              where: {
                '$schools.id$': req.params.id,
                '$schools.school.status$': status
              }
            })
          }
          return res.send({students: students.rows, totalStudents: students.count})
        } else if (req.query.lastName) {
          const {Op} = db.sequelize
          const students = await db.User.findAll({
            attributes: userAttributesSetup(),
            include: [{
              model: db.EducationalUnit,
              as: 'schools',
              attributes: ['id'],
              through: {
                as: 'school',
                attributes: ['status']
              }
            }],
            where: {
              '$schools.id$': req.params.id,
              '$schools.school.status$': 'active',
              lastName: {
                [Op.iLike]: `%${req.query.lastName}%`
              }
            },
            order: [['lastName', 'ASC']]
          })
          res.send({students})
        }
      }
    } catch (error) {
      res.status(400).send({error})
    }
  },

  async fetchTeacherCourses (req, res) {
    try {
      const {status} = req.query
      let courses
      if (status === 'all') {
        courses = await db.Course.findAll({
          include: [
            {
              model: db.Subject,
              as: 'Subject',
              attributes: ['name']
            }, {
              model: db.User,
              as: 'students',
              attributes: userAttributesSetup()
            }
          ],
          where: {
            teacherId: req.params.userId
          }
        })
        return res.send({courses})
      }
      courses = await db.Course.findAll({
        include: [
          {
            model: db.Subject,
            as: 'Subject',
            attributes: ['name']
          }, {
            model: db.User,
            as: 'students',
            attributes: userAttributesSetup()
          }
        ],
        where: {
          isActive: status === 'active',
          teacherId: req.params.userId,
          educationalUnitId: req.params.id
        }
      })
      res.send({courses})
    } catch (error) {
      res.status(400).send({error})
    }
  },

  async fetchTeachers (req, res) {
    try {
      const unit = await db.EducationalUnit.findById(req.params.id, {
        attributes: ['id'],
        include: [{
          model: db.User,
          as: 'teachers',
          attributes: ['id', 'firstName', 'lastName'],
          through: {
            where: {
              educationalUnitId: req.params.id,
              status: 'active'
            }
          }
        }]
      })
      res.send({teachers: unit.dataValues.teachers})
    } catch (error) {
      res.status(400).send({error})
    }
  },

  async fetchUnit (req, res) {
    try {
      const unit = await db.EducationalUnit.findById(req.params.id, {include: eduUnitIncludes(req.params.id)})
      res.send({unit})
    } catch (error) {
      res.status(400).send({error})
    }
  },

  async fetchUnits (req, res) {
    try {
      const {name, email, city, region, address, types} = req.query
      const {Op} = db.sequelize
      let conditionals
      if (address) {
        conditionals = {
          [Op.and]: [
            {name: {[Op.iLike]: `%${name}%`}},
            {email: {[Op.iLike]: `${email}%`}},
            {city: {[Op.iLike]: `${city}%`}},
            {region: {[Op.iLike]: `${region}%`}},
            {address: {[Op.iLike]: `%${address}%`}},
            {type: {[Op.in]: types.split(',')}}
          ]
        }
      } else {
        conditionals = {
          [Op.and]: [
            {name: {[Op.iLike]: `%${name}%`}},
            {email: {[Op.iLike]: `${email}%`}},
            {city: {[Op.iLike]: `${city}%`}},
            {region: {[Op.iLike]: `${region}%`}},
            {type: {[Op.in]: types.split(',')}}
          ]
        }
      }
      const units = await db.EducationalUnit.findAndCountAll({
        attributes: {exclude: ['logo']},
        where: conditionals,
        limit: req.query.rows,
        offset: req.query.page * req.query.rows
      })
      res.send({units: units.rows, totalUnits: units.count})
    } catch (error) {
      res.status(400).send({error})
    }
  },

  async updateUnit (req, res) {
    try {
      if (req.body.file) {
        req.body['logo'] = Buffer.from(req.body.file, 'base64')
      }
      const updateData = getUpdateData(req.body)
      await db.EducationalUnit.update(updateData, {where: {id: req.params.id}})
      const unit = await db.EducationalUnit.findById(req.params.id, {include: eduUnitIncludes(req.params.id)})
      res.send({
        unit,
        message: req.i18n.t('messages.success.eduUnitUpdatingCompleted'),
        type: 'success'
      })
    } catch (error) {
      res.status(400).send({error: getErrorMessages(error)})
    }
  }
}
