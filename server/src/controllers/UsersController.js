const {mailerKeys} = require('../config')
const mailer = require('../config/nodemailer')
const mails = require('../config/mails')
const db = require('../models')
const {createUserRoles, generateValidityToken, getErrorMessages, filterNotAdultChildren, getUpdateData,
  lessonScheduleIncludes, userAttributesSetup, userIncludes} = require('./helpers')

module.exports = {
  async createChildAccount (req, res) {
    try {
      if (req.body.username === '') {
        const maxId = await db.User.max('id')
        req.body.username = `user000${maxId + 1}`
      }
      const child = await db.User.create({
        username: req.body.username || null,
        email: req.body.email || null,
        password: req.body.password || null,
        firstName: req.body.firstName || null,
        lastName: req.body.lastName || null,
        birthday: req.body.date || null,
        city: req.body.city || null,
        region: req.body.region || null,
        phone: req.body.phone || null,
        validityToken: generateValidityToken(req.body.username) || null,
        parentId: req.params.id | null,
        inserterId: req.params.id | null,
        updaterId: req.params.id | null
      })
      const {id, username, email, firstName, lastName, validityToken} = child.dataValues
      await createUserRoles(['student'], id)
      await mailer.sendMail({
        from: `"SOZI" <${mailerKeys.user}>`,
        to: req.body.parent.email,
        subject: mails.registrationChildAccountParentMessage.subject(req.i18n),
        html: mails.registrationChildAccountParentMessage.html({
          firstName: req.body.parent.firstName,
          lastName: req.body.parent.lastName,
          childFirstName: firstName,
          childLastName: lastName,
          childUsername: username,
          validityToken,
          i18n: req.i18n
        })
      })
      await mailer.sendMail({
        from: `"SOZI" <${mailerKeys.user}>`,
        to: email,
        subject: mails.registrationChildAccountChildMessage.subject(req.i18n),
        html: mails.registrationChildAccountChildMessage.html({
          parentFirstName: req.body.parent.firstName,
          parentLastName: req.body.parent.lastName,
          firstName,
          lastName,
          username,
          validityToken,
          i18n: req.i18n
        })
      })
      res.send({
        child,
        message: req.i18n.t('messages.success.childRegistrationCompleted'),
        type: 'success'
      })
    } catch (error) {
      const messages = {}
      if (error.name === 'ErrorEmptyRole') {
        messages[error.path] = error.message
      }
      res.status(400).send({error: getErrorMessages(error, messages)})
    }
  },

  async fetchChildren (req, res) {
    try {
      const children = await db.User.findAndCountAll({
        attributes: userAttributesSetup(),
        include: [{
          model: db.Role,
          attributes: ['name'],
          through: {
            attributes: ['isValid', 'roleId']
          }
        }],
        where: {
          parentId: req.params.id
        }
      })
      const notAdultChildren = filterNotAdultChildren(children.rows)
      res.send({children: notAdultChildren, totalChildren: notAdultChildren.length})
    } catch (error) {
      res.status(400).send({error})
    }
  },

  async fetchChildrenLessons (req, res) {
    try {
      const {startDate, endDate, status} = req.query
      const {Op} = db.sequelize
      if (status === 'all') {
        const lessons = await db.Lesson.findAll({
          include: lessonScheduleIncludes(),
          where: {
            '$student.parent_id$': req.params.id,
            date: {
              [Op.between]: [startDate, endDate]
            }
          }
        })
        const notAdultChildrenLessons = lessons
          .filter(lesson => !lesson.dataValues.student.dataValues.Roles
            .some(role => role.dataValues.UserRole.roleId === 3 && role.dataValues.UserRole.isValid === true))
        return res.send({lessons: notAdultChildrenLessons})
      }
      const lessons = await db.Lesson.findAll({
        include: lessonScheduleIncludes(),
        where: {
          '$student.parent_id$': req.params.id,
          isActive: status === 'active',
          date: {
            [Op.between]: [startDate, endDate]
          }
        }
      })
      res.send({lessons})
    } catch (error) {
      res.status(400).send({error})
    }
  },

  async fetchUser (req, res) {
    try {
      let students = null
      let teachers = null
      let position = 1
      const queriedUser = await db.User.findById(req.params.id, {
        attributes: userAttributesSetup(),
        include: userIncludes()
      })
      const unit = await queriedUser.getOwnedUnits({attributes: ['id', 'name'], raw: true})
      if (unit.length > 0) {
        position = Math.floor(Math.random() * (unit.length - 1 + 1))
        students = await db.StudentEducationalUnit.findAll({
          attributes: [
            'status',
            [db.sequelize.fn('COUNT', 'StudentEducationalUnit.status'), 'studentsQuantity']
          ],
          where: {
            educationalUnitId: unit[position].id
          },
          group: ['StudentEducationalUnit.status'],
          raw: true
        })
        teachers = await db.TeacherEducationalUnit.findAll({
          attributes: [
            'status',
            [db.sequelize.fn('COUNT', 'TeacherEducationalUnit.status'), 'teachersQuantity']
          ],
          where: {
            educationalUnitId: unit[position].id
          },
          group: ['TeacherEducationalUnit.status'],
          raw: true
        })
      }
      const children = filterNotAdultChildren(queriedUser.dataValues.children)
      delete queriedUser.dataValues.children
      if (queriedUser && queriedUser.dataValues.parentId) {
        const parent = await db.User.findById(queriedUser.dataValues.parentId, {
          attributes: userAttributesSetup()
        })
        const updater = await db.User.findById(queriedUser.dataValues.updaterId, {
          attributes: userAttributesSetup()
        })
        const user = {...queriedUser.dataValues, unit: unit[position], students, teachers, children, parent, updater}
        return res.send({user})
      }
      res.send({user: {...queriedUser.dataValues, unit: unit[position], students, teachers, children}})
    } catch (error) {
      res.status(400).send({error})
    }
  },

  async fetchUserLessons (req, res) {
    try {
      const {startDate, endDate, status} = req.query
      const {Op} = db.sequelize
      if (status === 'all') {
        const lessons = await db.Lesson.findAll({
          include: [
            {
              model: db.User,
              as: 'student',
              attributes: ['id', 'firstName', 'lastName', 'city']
            }, {
              model: db.Course,
              attributes: ['title'],
              include: [
                {
                  model: db.Subject,
                  attributes: ['name']
                }, {
                  model: db.EducationalUnit,
                  attributes: ['name']
                }, {
                  model: db.User,
                  as: 'Teacher',
                  attributes: ['firstName', 'lastName']
                }
              ]
            }
          ],
          where: {
            '$student.id$': req.params.id,
            date: {
              [Op.between]: [startDate, endDate]
            }
          }
        })
        return res.send({lessons})
      }
      const lessons = await db.Lesson.findAll({
        include: [
          {
            model: db.User,
            as: 'student',
            attributes: ['id', 'firstName', 'lastName', 'city']
          }, {
            model: db.Course,
            attributes: ['title'],
            include: [
              {
                model: db.Subject,
                attributes: ['name']
              }, {
                model: db.EducationalUnit,
                attributes: ['name']
              }
            ]
          }
        ],
        where: {
          '$student.id': req.params.id,
          isActive: status === 'active',
          date: {
            [Op.between]: [startDate, endDate]
          }
        }
      })
      res.send({lessons})
    } catch (error) {
      res.status(400).send({error})
    }
  },

  async fetchUserUnits (req, res) {
    try {
      const units = await db.EducationalUnit.findAll({
        attributes: {exclude: ['logo']},
        include: [{
          model: db.User,
          as: 'students',
          attributes: [],
          through: {
            as: 'student',
            attributes: ['status']
          }
        }],
        where: {
          '$students.id$': req.params.id,
          '$students.student.status$': 'active'
        }
      })
      res.send({units, totalUnits: units.length})
    } catch (error) {
      res.status(400).send({error})
    }
  },

  async updateUser (req, res) {
    try {
      const updateData = getUpdateData(req.body)
      if (!updateData['password']) {
        delete updateData['password']
      }
      await db.User.findById(req.params.id)
        .then(user => user.update(updateData))
      const updatedUser = await db.User.findById(req.params.id, {include: userIncludes()})
      if (updatedUser.dataValues.updaterId === updatedUser.dataValues.id) {
        return res.send({
          user: updatedUser,
          message: req.i18n.t('messages.success.updatingCompleted'),
          type: 'success'
        })
      }
      const parent = await db.User.findById(updatedUser.dataValues.parentId, {
        attributes: userAttributesSetup()
      })
      const updater = await db.User.findById(updatedUser.dataValues.updaterId, {
        attributes: userAttributesSetup()
      })
      const user = {...updatedUser.dataValues, parent, updater}
      res.send({
        user,
        message: req.i18n.t('messages.success.childUpdatingCompleted'),
        type: 'success'
      })
    } catch (error) {
      res.status(400).send({error: getErrorMessages(error)})
    }
  }
}
