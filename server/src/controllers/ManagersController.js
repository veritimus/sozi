const db = require('../models')
const {userAttributesSetup} = require('./helpers')

module.exports = {
  async fetchUnits (req, res) {
    try {
      const {offset, rows} = req.query
      if (offset === 'null' && rows === 'null') {
        const units = await db.EducationalUnit.findAll({
          attributes: {exclude: ['logo']},
          include: [{
            model: db.User,
            as: 'managers',
            attributes: []
          }, {
            model: db.User,
            as: 'teachers',
            attributes: userAttributesSetup(),
            through: {
              attributes: ['status'],
              where: {
                status: 'active'
              }
            }
          }],
          where: {
            '$managers.id$': req.params.id
          }
        })
        return res.send({units})
      }
      const unitsIdList = {}
      const unitsStudents = {}
      const unitsTeachers = {}
      const units = await db.EducationalUnit.findAndCountAll({
        attributes: {exclude: ['logo']},
        include: [{
          model: db.User,
          as: 'managers',
          attributes: []
        }],
        where: {
          '$managers.id$': req.params.id
        },
        limit: rows,
        offset: offset,
        subQuery: false
      })
      await Promise.all(units.rows.map(async unit => {
        const unitId = unit.dataValues.id
        unitsIdList[unitId] = {}
        unitsIdList[unitId].students = {active: [], pending: []}
        unitsIdList[unitId].teachers = {active: [], pending: []}
        const students = await unit.getStudents({attributes: userAttributesSetup()})
        const teachers = await unit.getTeachers({attributes: userAttributesSetup()})
        if (students.length > 0) {
          unitsIdList[unitId].students.active = students.filter(student => student.dataValues.StudentEducationalUnit.status === 'active').map(student => student.dataValues.id)
          unitsIdList[unitId].students.pending = students.filter(student => student.dataValues.StudentEducationalUnit.status === 'pending').map(student => student.dataValues.id)
        }
        if (teachers.length > 0) {
          unitsIdList[unitId].teachers.active = teachers.filter(teacher => teacher.dataValues.TeacherEducationalUnit.status === 'active').map(teacher => teacher.dataValues.id)
          unitsIdList[unitId].teachers.pending = teachers.filter(teacher => teacher.dataValues.TeacherEducationalUnit.status === 'pending').map(teacher => teacher.dataValues.id)
        }
        unitsStudents[unitId] = students
        unitsTeachers[unitId] = teachers
      }))
      res.send({units: units.rows, unitsIdList, unitsStudents, unitsTeachers, totalUnits: units.count})
    } catch (error) {
      console.log(error)
      res.status(400).send({error})
    }
  }
}
