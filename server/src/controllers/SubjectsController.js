const db = require('../models')

module.exports = {
  async fetchSubjects (req, res) {
    const {Op} = db.sequelize
    try {
      if (req.query.name) {
        const subjects = await db.Subject.findAll({
          where: {
            name: {
              [Op.like]: `%${req.query.name}%`
            }
          },
          limit: 10,
          order: [['name', 'ASC']]
        })
        res.send({subjects})
      } else {
        const subjects = await db.Subject.findAll({order: [['name', 'ASC']]})
        res.send({subjects})
      }
    } catch (error) {
      res.status(400).send({error})
    }
  }
}
