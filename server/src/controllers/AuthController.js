const passport = require('passport')
const jwt = require('jsonwebtoken')
const {userValidationKeys, mailerKeys} = require('../config')
const mailer = require('../config/nodemailer')
const mails = require('../config/mails')
const db = require('../models')
const {createUserRoles, getErrorMessages, generatePassword, generateValidityToken, userAttributesSetup} = require('./helpers')

module.exports = {
  async checkIfExist (req, res) {
    try {
      const {Op} = db.sequelize
      const user = await db.User.findOne({
        where: {
          [Op.or]: [
            {username: req.query.username},
            {email: req.query.email}
          ]
        }
      })
      if (user) {
        throw new Error(req.i18n.t('messages.error.userAlreadyExist'))
      }
      res.send({message: 'OK'})
    } catch (error) {
      res.status(400).send({error: error.message})
    }
  },

  login (req, res) {
    passport.authenticate('local', {session: false}, (error, user, info) => {
      if (error) {
        return res.status(500).send({error})
      }
      if (user) {
        req.login(user, {session: false}, error => {
          if (error) {
            return res.status(401).send({error})
          } else if (!user.dataValues.isActive) {
            return res.status(403).send({
              error: {
                code: 'ErrorUnverifiedUser',
                message: req.i18n.t('messages.error.unverifiedUser')
              }
            })
          }
          return res.send({user: user.toAuthJSON()})
        })
      }
      if (!user) {
        res.status(401).send({
          error: {
            code: 'ErrorInvalidCredentials',
            message: req.i18n.t('messages.error.invalidLoginCredentials')
          }
        })
      }
    })(req, res)
  },

  async register (req, res) {
    try {
      if (req.body.roles.length < 1) {
        let error = new Error()
        error.name = 'ErrorEmptyRole'
        error.message = req.i18n.t('messages.error.emptyRole')
        error.path = 'roles'
        throw error
      }
      const user = await db.User.create({
        username: req.body.username || null,
        email: req.body.email || null,
        password: req.body.password || null,
        firstName: req.body.firstName || null,
        lastName: req.body.lastName || null,
        city: req.body.city || null,
        region: req.body.region || null,
        phone: req.body.phone || null,
        validityToken: generateValidityToken(req.body.username) || null
      })
      const {id, email, firstName, lastName, validityToken} = user.dataValues
      await createUserRoles(req.body.roles, id)
      await mailer.sendMail({
        from: `"SOZI" <${mailerKeys.user}>`,
        to: email,
        subject: mails.registration.subject(req.i18n),
        html: mails.registration.html({firstName, lastName, validityToken, i18n: req.i18n})
      })
      res.send({
        message: req.i18n.t('messages.success.registrationCompleted'),
        type: 'success'
      })
    } catch (error) {
      const messages = {}
      if (error.name === 'ErrorEmptyRole') {
        messages[error.path] = error.message
      }
      res.status(400).send({error: getErrorMessages(error, messages)})
    }
  },

  async sendNewPassword (req, res) {
    try {
      const user = await db.User.findOne({
        include: [{
          model: db.Role,
          attributes: ['name'],
          through: {
            attributes: ['isValid']
          }
        }],
        where: {username: req.body.username}
      })
      if (!user) {
        throw new Error(req.i18n.t('messages.error.userNotExists'))
      }
      const {firstName, lastName, email} = user.dataValues
      const password = generatePassword()
      await user.update({password})
      await mailer.sendMail({
        from: '"SOZI" <sozi.us.2018@gmail.com>',
        to: email,
        subject: mails.sendingPassword.subject(req.i18n),
        html: mails.sendingPassword.html({firstName, lastName, password, i18n: req.i18n})
      })
      if (user.dataValues.parentId && !user.dataValues.Roles.some(role => role.dataValues.name === 'parent' && role.dataValues.UserRole.isValid)) {
        const parent = await db.User.findOne({attributes: userAttributesSetup(), where: {id: user.dataValues.parentId}})
        await mailer.sendMail({
          from: '"SOZI" <sozi.us.2018@gmail.com>',
          to: parent.dataValues.email,
          subject: mails.sendingChildPassword.subject(req.i18n),
          html: mails.sendingChildPassword.html({firstName: parent.dataValues.firstName, lastName: parent.dataValues.lastName, childFirstName: firstName, childLastName: lastName, password, i18n: req.i18n})
        })
      }
      res.send({
        message: req.i18n.t('messages.success.passwordGenerationCompleted'),
        type: 'success'
      })
    } catch (error) {
      res.status(404).send({error: error.message})
    }
  },

  async sendNewValidityToken (req, res) {
    try {
      const user = await db.User.findOne({where: {email: req.body.email}})
      if (!user) {
        throw new Error(req.i18n.t('messages.error.emailNotExists'))
      }
      const {username, firstName, lastName, isActive} = user.dataValues
      if (isActive) {
        throw new Error(req.i18n.t('messages.error.accountAlreadyActive'))
      }
      const token = generateValidityToken(username)
      await user.update({validityToken: token})
      await mailer.sendMail({
        from: '"SOZI" <sozi.us.2018@gmail.com>',
        to: req.body.email,
        subject: mails.sendingValidityToken.subject(req.i18n),
        html: mails.sendingValidityToken.html({firstName, lastName, token, i18n: req.i18n})
      })
      res.send({
        message: req.i18n.t('messages.success.tokenGenerationCompleted'),
        type: 'success'
      })
    } catch (error) {
      res.status(404).send({error: error.message})
    }
  },

  async verifyUser (req, res) {
    try {
      const decodedUser = jwt.verify(req.params.token, userValidationKeys.secret)
      const user = await db.User.findOne({where: {username: decodedUser.username}})
      await user.update({isActive: true, validityToken: null})
      res.send({
        message: req.i18n.t('messages.success.verificationCompleted'),
        type: 'success'
      })
    } catch (error) {
      if (error.name === 'TokenExpiredError') {
        return res.status(404).send({error: req.i18n.t('messages.error.tokenExpired')})
      }
      res.status(404).send({error: error.message})
    }
  }
}
