const db = require('../models')
const {getErrorMessages, getUpdateData, lessonIncludes} = require('./helpers')

module.exports = {
  async fetchLesson (req, res) {
    try {
      const lesson = await db.Lesson.findById(req.params.id, {include: lessonIncludes()})
      res.send({lesson})
    } catch (error) {
      res.send({error})
    }
  },

  async updateLesson (req, res) {
    try {
      const updateData = getUpdateData(req.body)
      await db.Lesson.update(updateData, {where: {id: req.params.id}})
      const lesson = await db.Lesson.findById(req.params.id, {include: lessonIncludes()})
      res.send({
        lesson,
        message: req.i18n.t('messages.success.lessonUpdatingCompleted'),
        type: 'success'
      })
    } catch (error) {
      res.status(400).send({error: getErrorMessages(error)})
    }
  }
}
