const db = require('../models')
const {userAttributesSetup} = require('./helpers')

module.exports = {
  async fetchStudents (req, res) {
    try {
      const {firstName, lastName, email, city, region} = req.query
      const {Op} = db.sequelize
      const conditionals = {
        [Op.and]: [
          {firstName: {[Op.iLike]: `%${firstName}%`}},
          {lastName: {[Op.iLike]: `%${lastName}%`}},
          {email: {[Op.iLike]: `${email}%`}},
          {city: {[Op.iLike]: `${city}%`}},
          {region: {[Op.iLike]: `${region}%`}},
          {
            [Op.or]: [
              {'$Roles.name$': 'student'},
              {'$Roles.name$': 'parent'}
            ]
          }
        ]
      }
      const students = await db.User.findAndCountAll({
        attributes: userAttributesSetup(),
        include: [{
          model: db.Role,
          attributes: ['name']
        }],
        where: conditionals,
        limit: req.query.rows,
        offset: req.query.page * req.query.rows,
        subQuery: false
      })
      res.send({students: students.rows, totalStudents: students.count})
    } catch (error) {
      res.status(400).send({error})
    }
  }
}
