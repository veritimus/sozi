const db = require('../models')
const {userAttributesSetup} = require('./helpers')

module.exports = {
  async fetchStudents (req, res) {
    try {
      const {firstName, lastName, email, city, region} = req.query
      const {Op} = db.sequelize
      const conditionals = {
        [Op.and]: [
          {firstName: {[Op.iLike]: `%${firstName}%`}},
          {lastName: {[Op.iLike]: `%${lastName}%`}},
          {email: {[Op.iLike]: `${email}%`}},
          {city: {[Op.iLike]: `${city}%`}},
          {region: {[Op.iLike]: `${region}%`}},
          {parentId: {[Op.not]: null}}
        ]
      }
      const students = await db.User.findAndCountAll({
        attributes: userAttributesSetup(),
        where: conditionals,
        limit: req.query.rows,
        offset: req.query.offset
      })
      res.send({students: students.rows, totalStudents: students.count})
    } catch (error) {
      res.send({error})
    }
  }
}
