module.exports = (sequelize, DataTypes) => {
  const Subject = sequelize.define('Subject', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      unique: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(60),
      allowNull: false,
      unique: {
        msg: 'This subject already exists.'
      },
      validate: {
        notNull: {
          msg: 'Subject name must not be empty.'
        },
        len: {
          args: [2, 60],
          msg: 'Subject name must have 2-60 length.'
        }
      }
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true,
      validate: {
        len: {
          args: [0, 1000],
          msg: 'Description must have 0-1000 length.'
        }
      }
    }
  }, {
    underscored: true
  })

  return Subject
}
