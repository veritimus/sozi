module.exports = (sequelize, DataTypes) => {
  const Role = sequelize.define('Role', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      unique: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(15),
      allowNull: false,
      unique: {
        msg: 'This role already exists.'
      },
      validate: {
        notNull: {
          msg: 'Role name must not be empty.'
        }
      }
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
      field: 'is_active'
    }
  }, {
    underscored: true
  })

  Role.associate = ({Role, User, UserRole}) => {
    Role.belongsToMany(User, {
      through: UserRole,
      foreignKey: {
        name: 'roleId',
        allowNull: false,
        field: 'role_id'
      }
    })
  }

  return Role
}
