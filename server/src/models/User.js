const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const {authKeys} = require('../config')

function hashPassword (user, options) {
  if (!user.changed('password')) {
    return
  }

  return bcrypt.hash(user.password, parseInt(authKeys.saltFactor))
    .then(hashedPassword => { user.password = hashedPassword })
    .catch(error => { throw new Error(error) })
}

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      unique: true,
      primaryKey: true
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: {
        msg: 'This username is already used.'
      },
      validate: {
        notNull: {
          msg: 'User username must not be empty.'
        },
        isAlphanumeric: {
          msg: 'Username must be alphanumeric. Valid example: user12345.'
        },
        len: {
          args: [8, 255],
          msg: 'Username must have 8-255 length.'
        }
      }
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: {
        msg: 'This e-mail is already used.'
      },
      validate: {
        notNull: {
          msg: 'User e-mail must not be empty.'
        },
        isEmail: {
          msg: 'Invalid e-mail. Valid example: user123@gmail.com.'
        },
        len: {
          args: [8, 255],
          msg: 'E-mail must have 8-255 length.'
        }
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'User password must not be empty.'
        },
        len: {
          args: [8, 245],
          msg: 'Password must have 8-245 length.'
        }
      }
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'User first name must not be empty.'
        },
        is: {
          args: /^(([^\u0000-\u007F]|[^\d]){1}([\s'-]){0,1})+$/i,
          msg: `Allowed characters are letters, space, ' and -. Valid example: John.`
        },
        len: {
          args: [2, 255],
          msg: 'First name must have 2-255 length.'
        }
      },
      field: 'first_name'
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'User last name must not be empty.'
        },
        is: {
          args: /^(([^\u0000-\u007F]|[^\d]){1}([\s'-]){0,1})+$/i,
          msg: `Allowed characters are letters, space, ' and -. Valid example: Doe.`
        },
        len: {
          args: [2, 255],
          msg: 'Last name must have 2-255 length.'
        }
      },
      field: 'last_name'
    },
    city: {
      type: DataTypes.STRING(60),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'User city must not be empty.'
        },
        is: {
          args: /^(([^\u0000-\u007F]|[^\d]){1}([\s'-]){0,1})+$/i,
          msg: `Allowed characters are letters, space, ' and -. Valid example: London.`
        },
        len: {
          args: [2, 60],
          msg: 'City must have 2-60 length.'
        }
      }
    },
    region: {
      type: DataTypes.STRING(60),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'You must select region.'
        },
        is: {
          args: /^(([^\u0000-\u007F]|\w){1}([\s'-]){0,1})+$/i,
          msg: `Allowed characters are letters, space, ' and -. Valid example: warmińsko-mazurskie.`
        },
        len: {
          args: [2, 60],
          msg: 'Region must have 2-60 length.'
        }
      }
    },
    registrationDate: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      defaultValue: DataTypes.NOW,
      field: 'registration_date'
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
      field: 'is_active'
    },
    phone: {
      type: DataTypes.STRING(15),
      allowNull: true,
      validate: {
        is: {
          args: /^[0-9]+$/i,
          msg: `Phone number must be numeric.`
        },
        len: {
          args: [9, 15],
          msg: 'Phone number must have 9-15 length.'
        }
      }
    },
    birthday: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    validityToken: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'validity_token'
    }
  }, {
    underscored: true
  })

  User.authentication = async function ({username, password}) {
    const user = await User.findOne({
      where: {
        'username': username
      }
    })

    if (user) {
      const match = await bcrypt.compare(password, user.dataValues.password)
      if (match) {
        return user
      }
    }

    return null
  }
  User.beforeCreate((user, options) => hashPassword(user, options))
  User.beforeUpdate((user, options) => hashPassword(user, options))
  User.prototype.generateJWT = function () {
    return jwt.sign({
      id: this.id,
      username: this.username
    }, authKeys.secret, {expiresIn: `${authKeys.tokenValidityTime}s`})
  }
  User.prototype.toAuthJSON = function () {
    return {
      id: this.id,
      username: this.username,
      token: this.generateJWT(),
      expiresIn: authKeys.tokenValidityTime
    }
  }

  User.associate = ({
    User,
    Role,
    Course,
    Lesson,
    EducationalUnit,
    UserRole,
    StudentCourse,
    ManagerEducationalUnit,
    TeacherEducationalUnit,
    StudentEducationalUnit
  }) => {
    User.hasMany(User, {
      as: {
        singular: 'child',
        plural: 'children'
      },
      foreignKey: {
        name: 'parentId',
        allowNull: true,
        field: 'parent_id'
      }
    })
    User.hasOne(User, {
      as: 'Inserter',
      foreignKey: {
        name: 'inserterId',
        allowNull: true,
        field: 'inserter_id'
      }
    })
    User.hasOne(User, {
      as: 'Updater',
      foreignKey: {
        name: 'updaterId',
        allowNull: true,
        field: 'updater_id'
      }
    })
    User.hasMany(Course, {
      foreignKey: {
        name: 'teacherId',
        allowNull: false,
        field: 'teacher_id'
      }
    })
    User.hasMany(Lesson, {
      foreignKey: {
        name: 'studentId',
        allowNull: false,
        field: 'student_id'
      }
    })
    User.belongsToMany(Role, {
      through: UserRole,
      foreignKey: {
        name: 'userId',
        allowNull: false,
        field: 'user_id'
      }
    })
    User.belongsToMany(Course, {
      through: StudentCourse,
      as: 'courses',
      foreignKey: {
        name: 'studentId',
        allowNull: false,
        field: 'student_id'
      }
    })
    User.belongsToMany(EducationalUnit, {
      through: StudentEducationalUnit,
      as: 'schools',
      foreignKey: {
        name: 'studentId',
        allowNull: false,
        field: 'student_id'
      }
    })
    User.belongsToMany(EducationalUnit, {
      through: ManagerEducationalUnit,
      as: 'ownedUnits',
      foreignKey: {
        name: 'managerId',
        allowNull: false,
        field: 'manager_id'
      }
    })
    User.belongsToMany(EducationalUnit, {
      through: TeacherEducationalUnit,
      as: 'workUnits',
      foreignKey: {
        name: 'teacherId',
        allowNull: false,
        field: 'teacher_id'
      }
    })
  }

  return User
}
