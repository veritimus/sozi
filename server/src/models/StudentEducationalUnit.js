module.exports = (sequelize, DataTypes) => {
  const StudentEducationalUnit = sequelize.define('StudentEducationalUnit', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      unique: true,
      primaryKey: true
    },
    status: {
      type: DataTypes.ENUM('active', 'inactive', 'pending'),
      allowNull: false,
      defaultValue: 'pending',
      validate: {
        notNull: {
          msg: 'You must select student status.'
        }
      }
    },
    updaterId: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: 'updater_id'
    }
  }, {
    underscored: true
  })

  return StudentEducationalUnit
}
