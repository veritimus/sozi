const Sequelize = require('sequelize')
const config = require('../config')
let sequelize

Sequelize.Validator.notNull = function (item) {
  return !this.isNull(item)
}

if (process.env.NODE_ENV === 'production') {
  sequelize = new Sequelize(process.env.DATABASE_URL)
} else {
  sequelize = new Sequelize(
    config.db.database,
    config.db.user,
    config.db.password, {
      dialect: config.db.dialect,
      host: config.db.host,
      operatorsAliases: Sequelize.Op
    }
  )
}

const models = {
  User: sequelize.import('./User'),
  Role: sequelize.import('./Role'),
  Course: sequelize.import('./Course'),
  EducationalUnit: sequelize.import('./EducationalUnit'),
  Lesson: sequelize.import('./Lesson'),
  Subject: sequelize.import('./Subject'),
  UserRole: sequelize.import('./UserRole'),
  StudentCourse: sequelize.import('./StudentCourse'),
  ManagerEducationalUnit: sequelize.import('./ManagerEducationalUnit'),
  TeacherEducationalUnit: sequelize.import('./TeacherEducationalUnit'),
  StudentEducationalUnit: sequelize.import('./StudentEducationalUnit')
}

Object.keys(models).forEach(key => {
  if ('associate' in models[key]) {
    models[key].associate(models)
  }
})

models.sequelize = sequelize
models.Sequelize = Sequelize

module.exports = models
