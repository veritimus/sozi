const {DateTime} = require('luxon')

function setEndDate (lesson, options) {
  if (!lesson.changed('date')) {
    return
  }
  lesson.endDate = DateTime.fromISO(lesson.date.toISOString(), {zone: 'Europe/Warsaw'}).plus({minutes: lesson.duration}).toISO()
}

module.exports = (sequelize, DataTypes) => {
  const Lesson = sequelize.define('Lesson', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      unique: true,
      primaryKey: true
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
      field: 'is_active'
    },
    homework: {
      type: DataTypes.TEXT,
      allowNull: true,
      defaultValue: 'Repeat the lesson.',
      validate: {
        len: {
          args: [0, 1000],
          msg: 'Homework must have 0-1000 length.'
        }
      }
    },
    teacherNotes: {
      type: DataTypes.TEXT,
      allowNull: true,
      defaultValue: 'No content',
      validate: {
        len: {
          args: [0, 1000],
          msg: 'Notes must have 0-1000 length.'
        }
      },
      field: 'teacher_notes'
    },
    date: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'You must select lesson date.'
        }
      }
    },
    endDate: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'You must select lesson date.'
        }
      }
    },
    duration: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 45,
      validate: {
        notNull: {
          msg: 'You must set lesson duration time.'
        },
        isInt: {
          msg: 'Lesson duration time must be integer.'
        },
        min: 0,
        max: 1000
      }
    },
    price: {
      type: DataTypes.DECIMAL(7, 2),
      allowNull: false,
      defaultValue: 0.00,
      validate: {
        notNull: {
          msg: 'Lesson price must not be empty.'
        },
        isDecimal: {
          msg: 'Price must be decimal. Valid example: 9.99'
        },
        min: 0.00,
        max: 9999.99
      }
    },
    paymentStatus: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
      field: 'payment_status'
    },
    paymentDeadline: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'You must select lesson payment deadline.'
        }
      },
      field: 'payment_deadline'
    },
    messageForParent: {
      type: DataTypes.TEXT,
      allowNull: true,
      validate: {
        len: {
          args: [0, 1000],
          msg: 'Message must have 0-1000 length.'
        }
      },
      field: 'message_for_parent'
    },
    messageForTeacher: {
      type: DataTypes.TEXT,
      allowNull: true,
      validate: {
        len: {
          args: [0, 1000],
          msg: 'Message must have 0-1000 length.'
        }
      },
      field: 'message_for_teacher'
    }
  }, {
    underscored: true,
    hooks: {
      beforeValidate: setEndDate,
      beforeCreate: async function (lesson) {
        const existedLesson = await sequelize.models.Lesson.find({
          where: {
            studentId: lesson.studentId,
            [sequelize.Op.or]: {
              date: {
                [sequelize.Op.between]: [lesson.date, DateTime.fromISO(lesson.date.toISOString(), {zone: 'Europe/Warsaw'}).plus({minutes: lesson.duration}).toISO()]
              },
              endDate: {
                [sequelize.Op.between]: [lesson.date, DateTime.fromISO(lesson.date.toISOString(), {zone: 'Europe/Warsaw'}).plus({minutes: lesson.duration}).toISO()]
              }
            }
          }
        })
        if (existedLesson) {
          const error = new Error()
          error.name = 'UserDateNotAvailable'
          throw error
        }
      },
      beforeUpdate: setEndDate
    }
  })

  Lesson.associate = ({Lesson, Course, User}) => {
    Lesson.belongsTo(Course, {
      foreignKey: {
        name: 'courseId',
        allowNull: false,
        field: 'course_id'
      },
      targetKey: 'id'
    })
    Lesson.belongsTo(User, {
      as: 'student',
      foreignKey: {
        name: 'studentId',
        allowNull: false,
        field: 'student_id'
      },
      targetKey: 'id'
    })
    Lesson.belongsTo(User, {
      as: 'updater',
      foreignKey: {
        name: 'updaterId',
        allowNull: true,
        field: 'updater_id'
      },
      targetKey: 'id'
    })
  }

  return Lesson
}
