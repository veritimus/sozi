module.exports = (sequelize, DataTypes) => {
  const Course = sequelize.define('Course', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      unique: true,
      primaryKey: true
    },
    defaultDuration: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 45,
      validate: {
        notNull: {
          msg: 'You must set default duration time.'
        },
        isInt: {
          msg: 'Lesson default duration must be integer.'
        },
        min: 0,
        max: 1000
      },
      field: 'default_duration'
    },
    defaultPrice: {
      type: DataTypes.DECIMAL(7, 2),
      allowNull: false,
      defaultValue: 0.00,
      validate: {
        notNull: {
          msg: 'You must set default lesson price.'
        },
        isDecimal: {
          msg: 'Price must be decimal. Valid example: 9.99'
        },
        min: 0.00,
        max: 9999.99
      },
      field: 'default_price'
    },
    defaultDay: {
      type: DataTypes.ARRAY(DataTypes.ENUM(
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
        'Sunday'
      )),
      allowNull: true
    },
    defaultTime: {
      type: DataTypes.STRING(5),
      allowNull: false,
      defaultValue: '16:00',
      validate: {
        is: {
          args: /^(20|21|22|23|[0-1][0-9]):[0-5][0-9]$/,
          msg: 'Invalid time format. Valid example: 18:45.'
        }
      },
      field: 'default_time'
    },
    lessonsQuantity: {
      type: DataTypes.INTEGER,
      allowNull: true,
      validate: {
        isInt: {
          msg: 'Lessons quantity must be integer.'
        },
        min: 0,
        max: 1000
      },
      field: 'lessons_quantity'
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Course title must not be empty.'
        },
        len: {
          args: [5, 255],
          msg: 'Title must have 5-255 length.'
        }
      }
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Course description must not be empty.'
        },
        len: {
          args: [5, 1000],
          msg: 'Description must have 5-1000 length.'
        }
      }
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
      field: 'is_active'
    }
  }, {
    underscored: true
  })

  Course.associate = ({Course, User, EducationalUnit, Lesson, Subject, StudentCourse}) => {
    Course.belongsTo(EducationalUnit, {
      foreignKey: {
        name: 'educationalUnitId',
        allowNull: false,
        field: 'educational_unit_id'
      },
      targetKey: 'id'
    })
    Course.belongsTo(User, {
      as: 'Teacher',
      foreignKey: {
        name: 'teacherId',
        allowNull: false,
        field: 'teacher_id'
      },
      targetKey: 'id'
    })
    Course.belongsTo(Subject, {
      foreignKey: {
        name: 'subjectId',
        allowNull: false,
        field: 'subject_id'
      },
      targetKey: 'id'
    })
    Course.hasMany(Lesson, {
      foreignKey: {
        name: 'courseId',
        allowNull: false,
        field: 'course_id'
      }
    })
    Course.belongsToMany(User, {
      through: StudentCourse,
      as: 'students',
      foreignKey: {
        name: 'courseId',
        allowNull: false,
        field: 'course_id'
      }
    })
  }

  return Course
}
