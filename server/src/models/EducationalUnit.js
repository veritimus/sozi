module.exports = (sequelize, DataTypes) => {
  const EducationalUnit = sequelize.define('EducationalUnit', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      unique: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: {
        msg: 'School with this name already exists.'
      },
      validate: {
        notNull: {
          msg: 'Unit name must not be empty.'
        },
        len: {
          args: [3, 255],
          msg: 'School name must have 3-255 length.'
        }
      }
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: {
        msg: 'School with this e-mail already exists.'
      },
      validate: {
        notNull: {
          msg: 'Unit e-mail must not be empty.'
        },
        isEmail: {
          msg: 'Invalid e-mail. Valid example: schoolname@gmail.com.'
        },
        len: {
          args: [8, 255],
          msg: 'E-mail must have 8-255 length.'
        }
      }
    },
    city: {
      type: DataTypes.STRING(60),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Unit city must not be empty.'
        },
        is: {
          args: /^(([^\u0000-\u007F]|[^\d]){1}([\s'-]){0,1})+$/i,
          msg: `Allowed characters are letters, space, ' and -. Valid example: London.`
        },
        len: {
          args: [2, 60],
          msg: 'City must have 2-60 length.'
        }
      }
    },
    region: {
      type: DataTypes.STRING(60),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'You must select region.'
        },
        is: {
          args: /^(([^\u0000-\u007F]|\w){1}([\s'-]){0,1})+$/i,
          msg: `Allowed characters are letters, space, ' and -. Valid example: warmińsko-mazurskie.`
        },
        len: {
          args: [2, 60],
          msg: 'Region must have 2-60 length.'
        }
      }
    },
    type: {
      type: DataTypes.STRING(15),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'You must select unit type.'
        }
      }
    },
    bankAccount: {
      type: DataTypes.STRING(26),
      allowNull: true,
      validate: {
        is: {
          args: /^[0-9]+$/i,
          msg: 'Invalid bank account format. Bank account must contain only digits.'
        },
        len: {
          args: [26, 26],
          msg: 'Bank account must have 26 length.'
        }
      },
      field: 'bank_account'
    },
    logo: {
      type: DataTypes.BLOB('long'),
      allowNull: true,
      get () {
        if (this.getDataValue('logo') !== undefined) {
          const data = this.getDataValue('logo')
          return data ? data.toString('base64') : ''
        } else {
          return undefined
        }
      }
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true,
      validate: {
        len: {
          args: [0, 1000],
          msg: 'Description must have 0-1000 length.'
        }
      }
    },
    address: {
      type: DataTypes.STRING,
      allowNull: true,
      validate: {
        len: {
          args: [0, 255],
          msg: 'Address must have 0-255 length.'
        }
      }
    },
    website: {
      type: DataTypes.STRING,
      allowNull: true,
      validate: {
        isUrl: {
          msg: 'Invalid website address format. Valid example: http://school-address.org.'
        },
        is: {
          args: /^(http|https):\/\/[a-z0-9-.]+\.[^ "]+$/,
          msg: 'Invalid website address format. Valid example: http://school-address.org.'
        },
        len: {
          args: [12, 255],
          msg: 'Website must have 12-255 length.'
        }
      }
    },
    facebook: {
      type: DataTypes.STRING,
      allowNull: true,
      validate: {
        isUrl: {
          msg: 'Invalid facebook address format. Valid example: http://facebook.com/schoolname.'
        },
        is: {
          args: /^(http|https):\/\/[a-z0-9-.]+\.com\/[^ "]+$/,
          msg: 'Invalid facebook address format. Valid example: http://facebook.com/schoolname.'
        },
        len: {
          args: [15, 255],
          msg: 'Facebook address must have 15-255 length.'
        }
      }
    },
    phone: {
      type: DataTypes.STRING(15),
      allowNull: true,
      validate: {
        is: {
          args: /^[0-9]+$/i,
          msg: `Phone number must be numeric.`
        },
        len: {
          args: [9, 15],
          msg: 'Phone number must have 9-15 length.'
        }
      }
    }
  }, {
    underscored: true
  })

  EducationalUnit.associate = ({
    EducationalUnit,
    User,
    Course,
    ManagerEducationalUnit,
    TeacherEducationalUnit,
    StudentEducationalUnit
  }) => {
    EducationalUnit.hasMany(Course, {
      foreignKey: {
        name: 'educationalUnitId',
        allowNull: false,
        field: 'educational_unit_id'
      }
    })
    EducationalUnit.belongsToMany(User, {
      through: ManagerEducationalUnit,
      as: 'managers',
      foreignKey: {
        name: 'educationalUnitId',
        allowNull: false,
        field: 'educational_unit_id'
      }
    })
    EducationalUnit.belongsToMany(User, {
      through: TeacherEducationalUnit,
      as: 'teachers',
      foreignKey: {
        name: 'educationalUnitId',
        allowNull: false,
        field: 'educational_unit_id'
      }
    })
    EducationalUnit.belongsToMany(User, {
      through: StudentEducationalUnit,
      as: 'students',
      foreignKey: {
        name: 'educationalUnitId',
        allowNull: false,
        field: 'educational_unit_id'
      }
    })
  }

  return EducationalUnit
}
