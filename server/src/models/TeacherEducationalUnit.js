module.exports = (sequelize, DataTypes) => {
  const TeacherEducationalUnit = sequelize.define('TeacherEducationalUnit', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      unique: true,
      primaryKey: true
    },
    status: {
      type: DataTypes.ENUM('active', 'inactive', 'pending'),
      allowNull: false,
      defaultValue: 'pending',
      validate: {
        notNull: {
          msg: 'You must select teacher status.'
        }
      }
    },
    updaterId: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: 'updater_id'
    }
  }, {
    underscored: true
  })

  return TeacherEducationalUnit
}
