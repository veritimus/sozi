module.exports = (sequelize, DataTypes) => {
  const ManagerEducationalUnit = sequelize.define('ManagerEducationalUnit', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      unique: true,
      primaryKey: true
    }
  }, {
    underscored: true
  })

  return ManagerEducationalUnit
}
